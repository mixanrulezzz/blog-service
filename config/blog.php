<?php

return [
    'main' => [
        'per-page' => [
            'posts' => 10,
            'blogs' => 10,
            'comments' => 10,
        ],
    ],
];
