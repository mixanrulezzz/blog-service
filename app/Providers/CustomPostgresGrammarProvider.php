<?php

namespace App\Providers;

use App\Database\Schema\Grammar\PostgresGrammar;
use Illuminate\Support\ServiceProvider;

class CustomPostgresGrammarProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        \DB::connection('pgsql')->setSchemaGrammar(new PostgresGrammar());
    }
}
