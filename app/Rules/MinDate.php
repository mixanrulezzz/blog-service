<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\ValidationRule;

class MinDate implements ValidationRule, DataAwareRule
{
    /**
     * All of the data under validation.
     *
     * @var array<string, mixed>
     */
    protected $data = [];

    protected string $minDate;

    public function __construct(string $minDate)
    {
        $this->minDate = $minDate;
    }

    /**
     * Set the data under validation.
     *
     * @param  array<string, mixed>  $data
     */
    public function setData(array $data): static
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (empty($value)) {
            return;
        }

        $date = null;

        if ($this->minDate == 'now') {
            $date = \Date::now()->setTime(0, 0);
        } elseif (in_array($this->minDate, array_keys($this->data))) {
            if (empty($this->data[$this->minDate])) {
                return;
            }

            $date = \Date::createFromFormat('Y-m-d', $this->data[$this->minDate]);
        } elseif (strtotime($this->minDate) !== false) {
            $date = \Date::createFromFormat('Y-m-d', $this->minDate);
        } else {
            return;
        }

        if (\Date::createFromFormat('Y-m-d', $value)->setTime(0, 0) < $date) {
            $fail('validation.min_date')
                ->translate([
                    'attribute' => $attribute,
                    'date' => $date->format('Y-m-d'),
                ]);
        }
    }
}
