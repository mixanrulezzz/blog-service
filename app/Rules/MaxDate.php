<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\ValidationRule;

class MaxDate implements ValidationRule, DataAwareRule
{
    /**
     * All of the data under validation.
     *
     * @var array<string, mixed>
     */
    protected $data = [];

    protected string $maxDate;

    public function __construct(string $maxDate)
    {
        $this->maxDate = $maxDate;
    }

    /**
     * Set the data under validation.
     *
     * @param  array<string, mixed>  $data
     */
    public function setData(array $data): static
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (empty($value)) {
            return;
        }

        $date = null;

        if ($this->maxDate == 'now') {
            $date = \Date::now()->setTime(0, 0);
        } elseif (in_array($this->maxDate, array_keys($this->data))) {
            if (empty($this->data[$this->maxDate])) {
                return;
            }

            $date = \Date::createFromFormat('Y-m-d', $this->data[$this->maxDate]);
        } elseif (strtotime($this->maxDate) !== false) {
            $date = \Date::createFromFormat('Y-m-d', $this->maxDate);
        } else {
            return;
        }

        if (\Date::createFromFormat('Y-m-d', $value)->setTime(0, 0) > $date) {
            $fail('validation.min_date')
                ->translate([
                    'attribute' => $attribute,
                    'date' => $date->format('Y-m-d'),
                ]);
        }
    }
}
