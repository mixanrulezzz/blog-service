<?php

namespace App\Models;

use App\Services\Filters\QueryFilter;
use App\Services\Sort\QuerySort;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Route;

class Post extends Model
{
    use HasFactory;

    protected $touches = ['blog'];

    /**
     * Получить создателя поста
     * @return HasOne
     */
    public function owner(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'owner_id');
    }

    /**
     * Получить блог, к которому привязан пост
     * @return BelongsTo
     */
    public function blog(): BelongsTo
    {
        return $this->belongsTo(Blog::class, 'blog_id');
    }

    /**
     * Получить количество просмотров для поста
     * @return HasMany
     */
    public function views(): HasMany
    {
        return $this->hasMany(PostView::class, 'post_id');
    }

    /**
     * Получить количество лайков для поста
     * @return BelongsToMany
     */
    public function likes(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'post_likes', 'post_id', 'user_id')->withTimestamps();
    }

    /**
     * Получить комментарии
     * @return HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class, 'post_id');
    }

    /**
     * Применить фильтр
     * @param Builder $builder
     * @param QueryFilter $filter
     * @return Builder
     */
    public function scopeFilter(Builder $builder, QueryFilter $filter)
    {
        return $filter->apply($builder);
    }

    /**
     * Применить сортировку
     * @param Builder $builder
     * @param QuerySort $sort
     * @return Builder
     */
    public function scopeSort(Builder $builder, QuerySort $sort)
    {
        return $sort->apply($builder);
    }

    /**
     * @inheritDoc
     * @param mixed $value
     * @param null $field
     * @return Post|Model|null
     */
    public function resolveRouteBinding($value, $field = null)
    {
        return match (explode('@', Route::currentRouteAction())[1]) {
            'detail' => $this
                ->where('id', $value)
                ->select(['id', 'title', 'body', 'is_published', 'owner_id', 'blog_id'])
                ->withCount(['views', 'likes'])
                ->firstOrFail(),
            'edit' => $this
                ->where('id', $value)
                ->firstOrFail(['id', 'title', 'body', 'is_published', 'owner_id', 'blog_id']),
            'delete' => $this
                ->where('id', $value)
                ->firstOrFail(['id', 'owner_id']),
            'save' => $this
                ->where('id', $value)
                ->firstOrFail(['id', 'owner_id', 'published_at']),
            default => $this->where('id', $value)->firstOrFail(),
        };
    }

    public function scopeRelevanceCalc(Builder $builder)
    {
        $likesQueries = $viewsQueries = [];
        $lastDay = now()->addDays(-1);
        $lastHour = now()->addHours(-1);

        $likesQueries['all'] = PostLike::countPerPost();
        $likesQueries['lastHour'] = (clone $likesQueries['all'])->where('created_at', '>=', $lastHour->format($this->getDateFormat()));
        $likesQueries['lastDay'] = (clone $likesQueries['all'])->where('created_at', '>=', $lastDay->format($this->getDateFormat()));

        $viewsQueries['all'] = PostView::countPerPost();
        $viewsQueries['lastHour'] = (clone $viewsQueries['all'])->where('created_at', '>=', $lastHour->format($this->getDateFormat()));
        $viewsQueries['lastDay'] = (clone $viewsQueries['all'])->where('created_at', '>=', $lastDay->format($this->getDateFormat()));

        $builder->select([
            'posts.id as id',

            \DB::raw('MAX(likes.likes) as likes_count'),
            \DB::raw('MAX(views.views) as views_count'),

            \DB::raw('MAX(last_hour_likes.likes) as last_hour_likes_count'),
            \DB::raw('MAX(last_hour_views.views) as last_hour_views_count'),

            \DB::raw('MAX(last_day_likes.likes) as last_day_likes_count'),
            \DB::raw('MAX(last_day_views.views) as last_day_views_count'),
        ])
            ->leftJoinSub($likesQueries['all'], 'likes', fn($join) => $join->on('posts.id', '=', 'likes.post_id'))
            ->leftJoinSub($viewsQueries['all'], 'views', fn($join) => $join->on('posts.id', '=', 'views.post_id'))

            ->leftJoinSub($likesQueries['lastHour'], 'last_hour_likes', fn($join) => $join->on('posts.id', '=', 'last_hour_likes.post_id'))
            ->leftJoinSub($viewsQueries['lastHour'], 'last_hour_views', fn($join) => $join->on('posts.id', '=', 'last_hour_views.post_id'))

            ->leftJoinSub($likesQueries['lastDay'], 'last_day_likes', fn($join) => $join->on('posts.id', '=', 'last_day_likes.post_id'))
            ->leftJoinSub($viewsQueries['lastDay'], 'last_day_views', fn($join) => $join->on('posts.id', '=', 'last_day_views.post_id'))
            ->groupBy('posts.id');


        return $builder;
    }
}
