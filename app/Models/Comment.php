<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Route;

class Comment extends Model
{
    use HasFactory;

    protected $touches = ['post'];

    /**
     * Получить создателя коммента
     * @return HasOne
     */
    public function owner(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'owner_id');
    }

    /**
     * Получить пост, для которого сделан данный комментарий
     * @return HasOne
     */
    public function post(): HasOne
    {
        return $this->hasOne(Post::class, 'id', 'post_id');
    }

    /**
     * @inheritDoc
     * @param mixed $value
     * @param null $field
     * @return Model|null
     */
    public function resolveRouteBinding($value, $field = null)
    {
        return match (explode('@', Route::currentRouteAction())[1]) {
            'delete' => $this
                ->where('id', $value)
                ->firstOrFail(['id', 'owner_id']),
            'save' => $this
                ->where('id', $value)
                ->firstOrFail(['id', 'owner_id', 'post_id']),
            default => $this
                ->where('id', $value)
                ->firstOrFail(),
        };
    }
}
