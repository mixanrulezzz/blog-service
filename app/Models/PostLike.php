<?php

namespace App\Models;

use App\Traits\HasCompositePrimaryKey;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostLike extends Model
{
    use HasFactory, HasCompositePrimaryKey;

    protected $table = 'post_likes';

    protected $fillable = ['post_id', 'user_id'];

    protected $primaryKey = ['post_id', 'user_id'];

    public $incrementing = false;

    /**
     * Количество лайков в каждом из блогов
     * @param Builder $builder
     * @return Builder
     */
    public function scopeCountPerBlog(Builder $builder): Builder
    {
        return $builder->select([
                'posts.blog_id as blog_id',
                \DB::raw('COUNT(post_likes.user_id) as likes')
            ])
            ->join('posts', 'posts.id', '=', 'post_likes.post_id')
            ->groupBy(['posts.blog_id']);
    }

    /**
     * Количество лайков в каждом из постов
     * @param Builder $builder
     * @return Builder
     */
    public function scopeCountPerPost(Builder $builder): Builder
    {
        return $builder->select([
            'post_id',
            \DB::raw('COUNT(user_id) as likes'),
        ])
            ->groupBy(['post_id']);
    }
}
