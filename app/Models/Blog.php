<?php

namespace App\Models;

use App\Services\Filters\QueryFilter;
use App\Services\Sort\QuerySort;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Route;

class Blog extends Model
{
    use HasFactory;

    /**
     * Получить создателя блога
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function owner()
    {
        return $this->hasOne(User::class, 'id', 'owner_id');
    }

    /**
     * Получить авторов блога
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function authors()
    {
        return $this->belongsToMany(User::class, 'blog_authors', 'blog_id', 'author_id');
    }

    /**
     * Получить подписчиков блога
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function subscribers()
    {
        return $this->belongsToMany(User::class, 'subscriptions', 'blog_id', 'user_id')->withTimestamps();
    }

    /**
     * Получить посты блога
     * @return HasMany
     */
    public function posts(): HasMany
    {
        return $this->hasMany(Post::class, 'blog_id');
    }

    /**
     * Применить фильтр
     * @param Builder $builder
     * @param QueryFilter $filter
     * @return Builder
     */
    public function scopeFilter(Builder $builder, QueryFilter $filter)
    {
        return $filter->apply($builder);
    }

    /**
     * Применить сортировку
     * @param Builder $builder
     * @param QuerySort $sort
     * @return Builder
     */
    public function scopeSort(Builder $builder, QuerySort $sort)
    {
        return $sort->apply($builder);
    }

    /**
     * @inheritDoc
     * @param mixed $value
     * @param null $field
     * @return Model|null
     */
    public function resolveRouteBinding($value, $field = null)
    {
        return match (explode('@', Route::currentRouteAction())[1]) {
            'detail', 'subscribe' => $this
                ->where('id', $value)
                ->firstOrFail(['id']),
            'edit' => $this
                ->where('id', $value)
                ->firstOrFail(['id', 'title', 'description', 'owner_id']),
            'delete', 'save' => $this
                ->where('id', $value)
                ->firstOrFail(['id', 'owner_id']),
            default => $this
                ->where('id', $value)
                ->firstOrFail(),
        };
    }

    /**
     * Запрос для расчета релевантности блогов
     * @param Builder $builder
     * @return Builder
     */
    public function scopeRelevanceCalc(Builder $builder)
    {
        $likesQueries = $viewsQueries = [];
        $lastDay = now()->addDays(-1);
        $lastHour = now()->addHours(-1);

        $likesQueries['all'] = PostLike::countPerBlog();
        $likesQueries['lastHour'] = (clone $likesQueries['all'])->where('post_likes.created_at', '>=', $lastHour->format($this->getDateFormat()));
        $likesQueries['lastDay'] = (clone $likesQueries['all'])->where('post_likes.created_at', '>=', $lastDay->format($this->getDateFormat()));

        $viewsQueries['all'] = PostView::countPerBlog();
        $viewsQueries['lastHour'] = (clone $viewsQueries['all'])->where('post_views.created_at', '>=', $lastHour->format($this->getDateFormat()));
        $viewsQueries['lastDay'] = (clone $viewsQueries['all'])->where('post_views.created_at', '>=', $lastDay->format($this->getDateFormat()));

        $builder->select([
            'blogs.id as id',

            \DB::raw('COUNT(posts.id) as posts_count'),
            \DB::raw('MAX(likes.likes) as likes_count'),
            \DB::raw('MAX(views.views) as views_count'),

            \DB::raw('MAX(last_hour_likes.likes) as last_hour_likes_count'),
            \DB::raw('MAX(last_hour_views.views) as last_hour_views_count'),

            \DB::raw('MAX(last_day_likes.likes) as last_day_likes_count'),
            \DB::raw('MAX(last_day_views.views) as last_day_views_count'),
        ])
            ->leftJoin('posts', 'posts.blog_id', '=', 'blogs.id')
            ->leftJoinSub($likesQueries['all'], 'likes', fn($join) => $join->on('blogs.id', '=', 'likes.blog_id'))
            ->leftJoinSub($viewsQueries['all'], 'views', fn($join) => $join->on('blogs.id', '=', 'views.blog_id'))

            ->leftJoinSub($likesQueries['lastHour'], 'last_hour_likes', fn($join) => $join->on('blogs.id', '=', 'last_hour_likes.blog_id'))
            ->leftJoinSub($viewsQueries['lastHour'], 'last_hour_views', fn($join) => $join->on('blogs.id', '=', 'last_hour_views.blog_id'))

            ->leftJoinSub($likesQueries['lastDay'], 'last_day_likes', fn($join) => $join->on('blogs.id', '=', 'last_day_likes.blog_id'))
            ->leftJoinSub($viewsQueries['lastDay'], 'last_day_views', fn($join) => $join->on('blogs.id', '=', 'last_day_views.blog_id'))
            ->groupBy('blogs.id');

        return $builder;
    }
}
