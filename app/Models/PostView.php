<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostView extends Model
{
    use HasFactory;

    protected $table = 'post_views';

    protected $primaryKey = 'post_id';

    protected $fillable = ['post_id', 'ip'];

    /**
     * Количество просмотров в каждом из блогов
     * @param Builder $builder
     * @return Builder
     */
    public function scopeCountPerBlog(Builder $builder)
    {
        return $builder->select([
                'posts.blog_id as blog_id',
                \DB::raw('COUNT(post_views.ip) as views')
            ])
            ->join('posts', 'posts.id', '=', 'post_views.post_id')
            ->groupBy(['posts.blog_id']);
    }

    /**
     * Количество просмотров в каждом из постов
     * @param Builder $builder
     * @return Builder
     */
    public function scopeCountPerPost(Builder $builder): Builder
    {
        return $builder->select([
            'post_id',
            \DB::raw('COUNT(ip) as views'),
        ])
            ->groupBy(['post_id']);
    }
}
