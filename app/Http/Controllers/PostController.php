<?php

namespace App\Http\Controllers;

use App\Http\Requests\FilterSortRequest;
use App\Http\Requests\Post\LikePostRequest;
use App\Http\Requests\Post\SavePostRequest;
use App\Http\Requests\Post\ViewPostRequest;
use App\Models\Blog;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostView;
use App\Services\Filters\PostFilter;
use App\Services\Sort\PostSort;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class PostController extends Controller
{
    const POSTS_MAIN_CACHE_KEY = 'posts/main';

    /**
     * Главная страница приложения со списком опубликованных постов
     * @param FilterSortRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function list(FilterSortRequest $request)
    {
        $cacheTime = Carbon::now()->addHour();
        if (
            $request->hasAny(['word', 'date-from', 'date-to'])
            || $request->get('page', 1) != 1
            || $request->get('sort', 'published_at') != 'published_at'
            || $request->get('side', 'desc') != 'desc'
        ) {
            $cacheTime = null;
        }

        if ($cacheTime && \Cache::has(self::POSTS_MAIN_CACHE_KEY)) {
            $posts = \Cache::get(self::POSTS_MAIN_CACHE_KEY);
        } else {
            $postQuery = Post::where('is_published', true)
                ->select([
                    'id' => 'posts.id',
                    'title' => 'posts.title',
                    'is_published' => 'posts.is_published',
                    'created_at' => 'posts.created_at',
                    'published_at' => 'posts.published_at',
                    'owner_id' => 'posts.owner_id',
                ]);

            $filter = new PostFilter($request);
            $sort = new PostSort($request);

            $posts = $postQuery
                ->filter($filter)
                ->sort($sort)
                ->paginate(\Config::get('blog.main.per-page.posts', 10))
                ->withQueryString()
                ->onEachSide(1);

            if ($cacheTime) {
                \Cache::add(self::POSTS_MAIN_CACHE_KEY, $posts, $cacheTime);
            }
        }

        if ($cacheTime && $posts?->count() == 0) {
            \Cache::delete(self::POSTS_MAIN_CACHE_KEY);
        }

        return view('main', [
            'posts' => $posts,
            'sortList' => PostSort::getSortList(),
            'request' => $request,
        ]);
    }
    /**
     * Детальная страница поста
     * @param Post $post
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     * @see Post::resolveRouteBinding()
     */
    public function detail(Post $post)
    {
        $isLiked = false;
        if (!empty(\Auth::user())) {
            $isLiked = !empty(PostLike::where([
                ['post_id', '=', $post->id],
                ['user_id', '=', \Auth::id()],
            ])->first(['post_id', 'user_id']));
        }

        $comments = $post->comments()->paginate(\Config::get('blog.main.per-page.comments', 10));

        return view('post.detail', [
            'post' => $post,
            'isLiked' => $isLiked,
            'comments' => $comments,
        ]);
    }

    /**
     * Страница создания или изменения поста
     * @param Post|null $post
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     * @see Post::resolveRouteBinding()
     */
    public function edit(?Post $post = null)
    {
        if (isset($post) && $post->owner_id != \Auth::id() && !\Auth::user()->is_admin) {
            throw new HttpException(403);
        }

        if (\Auth::user()->is_admin) {
            $blogsAuthor = Blog::get(['id', 'title']);
        } else {
            $blogsAuthor = \Auth::user()->blogsAuthor()->get(['id', 'title']);
            $blogsOwner = \Auth::user()->blogsOwner()->get(['id', 'title']);
            $blogsAuthor = $blogsAuthor->merge($blogsOwner);
        }

        return view('post.edit', ['editPost' => $post, 'blogsAuthor' => $blogsAuthor]);
    }

    /**
     * Удаление поста
     * @param Post $post
     * @return \Illuminate\Http\Response
     * @see Post::resolveRouteBinding()
     */
    public function delete(Post $post)
    {
        if ($post->owner_id != \Auth::id() && !\Auth::user()->is_admin) {
            throw new HttpException(403);
        }

        $post->delete();

        return response()->noContent();
    }

    /**
     * Сохранение поста
     * @param Post $post
     * @param SavePostRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     * @see Post::resolveRouteBinding()
     */
    public function save(Post $post, SavePostRequest $request)
    {
        if ($post->exists && $post->owner_id != \Auth::id() && !\Auth::user()->is_admin) {
            throw new HttpException(403);
        }

        $post->title = $request->get('title');
        $post->body = $request->get('body');
        $post->is_published = $request->get('is_published') == 'true';
        $post->owner_id = $post->owner_id ?? \Auth::user()->id;
        $post->blog_id = $request->get('blog_id');

        if ($request->get('is_published') == 'true' && empty($post->published_at)) {
            $post->published_at = now(\Config::get('app.timezone', 'UTC'));
        }

        $post->save();

        return response(['url' => route('user.posts')]);
    }

    /**
     * Добавление еще одного просмотра в счетчик просмотра поста
     * @param ViewPostRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     */
    public function view(ViewPostRequest $request)
    {
        $postId = $request->get('post_id');
        $ip = $request->ip();

        try {
            $postView = new PostView([
                'post_id' => $postId,
                'ip' => $ip,
            ]);
            $postView->save();
        } catch (QueryException $e) {
            // Проверяем и не выдаем ошибку, если это дубликат
            $errorCode = $e->errorInfo[1];
            if ($errorCode != 7) {
                throw $e;
            }
        }

        $views = PostView::where('post_id', $postId)->groupBy('post_id')->count('ip');

        return response(['views' => $views]);
    }

    /**
     * Добавление или удаление лайка у поста
     * @param LikePostRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     */
    public function like(LikePostRequest $request)
    {
        $postId = $request->get('post_id');
        $userId = \Auth::id();
        $action = $request->get('action');

        if (empty($userId)) {
            throw new HttpException(403);
        }

        switch ($action) {
            case 'add':
                try {
                    $postLike = new PostLike([
                        'post_id' => $postId,
                        'user_id' => $userId,
                    ]);
                    $postLike->save();
                } catch (QueryException $e) {
                    // Проверяем и не выдаем ошибку, если это дубликат
                    $errorCode = $e->errorInfo[1];
                    if ($errorCode != 1062) {
                        throw $e;
                    }
                }
                break;
            case 'delete':
                $likeToDelete = PostLike::where([
                    ['post_id', '=', $postId],
                    ['user_id', '=', $userId],
                ])->first(['post_id', 'user_id']);

                if ($likeToDelete) {
                    $likeToDelete->delete();
                }
                break;
        }

        $likes = PostLike::where('post_id', $postId)->groupBy('post_id')->count('user_id');

        return response(['likes' => $likes]);
    }
}
