<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaveCommentRequest;
use App\Models\Comment;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CommentController extends Controller
{
    /**
     * Сохранить комментарий
     * @param Comment $comment
     * @param SaveCommentRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     * @see Comment::resolveRouteBinding()
     */
    public function save(Comment $comment, SaveCommentRequest $request)
    {
        if ($comment->exists && $comment->owner_id != \Auth::id() && !\Auth::user()->is_admin) {
            throw new HttpException(403);
        }

        $comment->post_id = $comment->post_id ?? $request->get('post-id');
        $comment->owner_id = $comment->owner_id ?? \Auth::id();
        $comment->body = $request->get('body');
        $comment->save();

        return response([]);
    }

    /**
     * Удалить комментарий
     * @param Comment $comment
     * @return \Illuminate\Http\Response
     * @see Comment::resolveRouteBinding()
     */
    public function delete(Comment $comment)
    {
        if ($comment->owner_id != \Auth::id() && !\Auth::user()->is_admin) {
            throw new HttpException(403);
        }

        $comment->delete();

        return response()->noContent();
    }
}
