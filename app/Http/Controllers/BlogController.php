<?php

namespace App\Http\Controllers;

use App\Http\Requests\Blog\SaveBlogRequest;
use App\Http\Requests\Blog\SubscribeBlogRequest;
use App\Http\Requests\FilterSortRequest;
use App\Http\Requests\SearchPageRequest;
use App\Models\Blog;
use App\Models\Post;
use App\Services\Filters\BlogFilter;
use App\Services\Sort\BlogSort;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\HttpException;

class BlogController extends Controller
{
    const BLOGS_LIST_CACHE_KEY = 'blogs/list';

    /**
     * Страница со списком блогов
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     */
    public function list(FilterSortRequest $request)
    {
        $cacheTime = Carbon::now()->addHour();
        if (
            $request->hasAny(['word', 'date-from', 'date-to'])
            || $request->get('page', 1) != 1
            || $request->get('sort', 'published_at') != 'published_at'
            || $request->get('side', 'desc') != 'desc'
        ) {
            $cacheTime = null;
        }

        if ($cacheTime && \Cache::has(self::BLOGS_LIST_CACHE_KEY)) {
            $blogs = \Cache::get(self::BLOGS_LIST_CACHE_KEY);
        } else {
            $blogsQuery = Blog::select([
                'id' => 'blogs.id',
                'title' => 'blogs.title',
                'description' => 'blogs.description',
                'created_at' => 'blogs.created_at',
                'updated_at' => 'blogs.updated_at',
                'owner_id' => 'blogs.owner_id',
            ]);

            $filter = new BlogFilter($request);
            $sort = new BlogSort($request);

            $blogs = $blogsQuery
                ->filter($filter)
                ->sort($sort)
                ->paginate(\Config::get('blog.main.per-page.blogs', 10))
                ->withQueryString()
                ->onEachSide(1);

            if ($cacheTime) {
                \Cache::add(self::BLOGS_LIST_CACHE_KEY, $blogs, $cacheTime);
            }
        }

        if ($cacheTime && $blogs?->count() == 0) {
            \Cache::delete(self::BLOGS_LIST_CACHE_KEY);
        }

        return view('blogs', [
            'blogs' => $blogs,
            'sortList' => BlogSort::getSortList(),
            'request' => $request,
        ]);
    }

    /**
     * Детальная страница блога
     * @param Blog $blog
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     * @see Blog::resolveRouteBinding()
     */
    public function detail(Blog $blog)
    {
        $posts = $blog->posts()
            ->where('is_published', true)
            ->select(['id', 'title', 'is_published', 'created_at', 'published_at', 'owner_id'])
            ->orderByDesc('published_at')
            ->paginate(\Config::get('blog.main.per-page.posts', 10));

        $isSubscribed = false;
        if (!empty(\Auth::user())) {
            $isSubscribed = !empty(\Auth::user()->subscriptions()->wherePivot('blog_id', $blog->id)->first(['blog_id']));
        }

        return view('blog.detail', ['posts' => $posts, 'blogId' => $blog->id, 'isSubscribed' => $isSubscribed]);
    }

    /**
     * Страница изменения блога
     * @param Blog|null $blog
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     * @see Blog::resolveRouteBinding()
     */
    public function edit(?Blog $blog = null)
    {
        if (isset($blog) && $blog->owner_id != \Auth::id() && !\Auth::user()->is_admin) {
            throw new HttpException(403);
        }

        return view('blog.edit', ['editBlog' => $blog]);
    }

    /**
     * Сохранить данные о блоге
     * @param Blog $blog
     * @param SaveBlogRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     * @see Blog::resolveRouteBinding()
     */
    public function save(Blog $blog, SaveBlogRequest $request)
    {
        if ($blog->exists && $blog->owner_id != \Auth::id() && !\Auth::user()->is_admin) {
            throw new HttpException(403);
        }

        $blog->title = $request->get('title');
        $blog->description = $request->get('description');
        $blog->owner_id = $blog->owner_id ?? \Auth::id();
        $blog->save();

        $blog->authors()->sync($request->get('authors'));

        \Cache::delete(self::BLOGS_LIST_CACHE_KEY);

        return response(['url' => route('user.blogs')]);
    }

    /**
     * Удаление блога
     * @param Blog $blog
     * @return \Illuminate\Http\Response
     * @see Blog::resolveRouteBinding()
     */
    public function delete(Blog $blog)
    {
        if ($blog->owner_id != \Auth::id() && !\Auth::user()->is_admin) {
            throw new HttpException(403);
        }

        $blog->delete();

        \Cache::delete(self::BLOGS_LIST_CACHE_KEY);

        return response()->noContent();
    }

    /**
     * Подписаться или отписаться от блога
     * @param Blog $blog
     * @param SubscribeBlogRequest $request
     * @return \Illuminate\Http\Response
     * @see Blog::resolveRouteBinding()
     */
    public function subscribe(Blog $blog, SubscribeBlogRequest $request)
    {
        $action = $request->get('action');

        switch ($action) {
            case 'subscribe':
                \Auth::user()->subscriptions()->attach([$blog->id]);
                break;
            case 'unsubscribe':
                \Auth::user()->subscriptions()->detach([$blog->id]);
                break;
        }

        return response()->noContent();
    }
}
