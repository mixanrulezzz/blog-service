<?php

namespace App\Http\Controllers;

use App\Http\Requests\Blog\SubscribeBlogRequest;

class UserBlogController extends Controller
{
    public function userPosts()
    {
        $userPosts = \Auth::user()
            ->posts()
            ->select(['id', 'title', 'is_published', 'created_at', 'published_at', 'owner_id'])
            ->orderBy('created_at', 'desc')
            ->paginate(\Config::get('blog.main.per-page.posts', 10))
            ->onEachSide(1);

        return view('user-blog.posts', ['userPosts' => $userPosts]);
    }

    public function userBlogs()
    {
        $userCreatedBlogs = \Auth::user()
            ->blogsOwner()
            ->select(['id', 'title', 'description', 'created_at', 'updated_at', 'owner_id'])
            ->orderBy('updated_at', 'desc')
            ->paginate(\Config::get('blog.main.per-page.blogs', 10))
            ->onEachSide(1);

        $userAuthorBlogs = \Auth::user()
            ->blogsAuthor()
            ->select(['id', 'title', 'description', 'created_at', 'updated_at', 'owner_id'])
            ->orderBy('updated_at', 'desc')
            ->paginate(\Config::get('blog.main.per-page.blogs', 15))
            ->onEachSide(1);

        return view('user-blog.blogs', [
            'userCreatedBlogs' => $userCreatedBlogs,
            'userAuthorBlogs' => $userAuthorBlogs,
        ]);
    }

    public function userSubscriptions()
    {
        $userSubscriptionBlogs = \Auth::user()
            ->subscriptions()
            ->select(['id', 'title', 'description', 'created_at' => 'blogs.created_at', 'updated_at' => 'blogs.updated_at', 'owner_id'])
            ->orderByDesc('updated_at')
            ->paginate(\Config::get('blog.main.per-page.blogs', 15))
            ->onEachSide(1);

        return view('user-blog.subscriptions', [
            'userSubscriptionBlogs' => $userSubscriptionBlogs,
        ]);
    }
}
