<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Models\User;
use Illuminate\Http\Request;

class AuthorsController extends Controller
{
    public function search(SearchRequest $request)
    {
        $searchResult = User::select([
            'users.id as id',
            'users.name as name',
            \DB::raw('ts_rank(name_tsvector.name_vector, plainto_tsquery(\'' . $request->get('word') . '\')) as name_tsrank')
        ])
            ->join('users_name_tsvector as name_tsvector', 'users.id', '=', 'name_tsvector.id')
            ->where('name_tsvector.name_vector', '@@', \DB::raw('plainto_tsquery(\'' . $request->get('word') . '\')'))
            ->limit(10)
            ->orderByDesc('name_tsrank')
            ->get();

        $authors = [];
        foreach ($searchResult as $item) {
            $authors[] = [
                'value' => $item['id'], 'text' => $item['name']
            ];
        }

        return response($authors);
    }
}
