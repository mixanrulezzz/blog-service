<?php

namespace App\Http\Requests\Post;

use App\Traits\AjaxRequest;
use Illuminate\Foundation\Http\FormRequest;

class ViewPostRequest extends FormRequest
{
    use AjaxRequest;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'post_id' => 'required|integer|exists:posts,id',
        ];
    }
}
