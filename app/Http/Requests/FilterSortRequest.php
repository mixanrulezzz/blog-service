<?php

namespace App\Http\Requests;

use App\Rules\MaxDate;
use App\Rules\MinDate;
use Illuminate\Foundation\Http\FormRequest;

class FilterSortRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'word' => 'nullable|string|min:3|max:255',
            'date-from' => ['nullable', 'date', new MaxDate('now')],
            'date-to' => ['nullable', 'date', new MaxDate('now'), new MinDate('date-from')],
            'sort' => 'nullable|string',
            'side' => 'nullable|string|in:asc,desc',
        ];
    }

    public function messages()
    {
        return [
            'word.min' => __('Please enter at least :min characters to start the search'),
            'word.max' => __('Too many characters'),
            'word.string' => __('Please enter at least 3 characters to start the search'),
        ];
    }
}
