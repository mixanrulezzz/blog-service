<?php

namespace App\Http\Requests\Blog;

use App\Traits\AjaxRequest;
use Illuminate\Foundation\Http\FormRequest;

class SaveBlogRequest extends FormRequest
{
    use AjaxRequest;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'string|required|max:255|min:3',
            'description' => 'string|required|min:3',
            'authors' => 'array|required|exists:users,id',
        ];
    }
}
