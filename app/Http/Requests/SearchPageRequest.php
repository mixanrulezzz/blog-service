<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchPageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'word' => 'string|required|min:3|max:255',
        ];
    }

    public function messages()
    {
        return [
            'word.min' => __('Please enter at least :min characters to start the search'),
            'word.max' => __('Too many characters'),
            'word.string' => __('Please enter at least 3 characters to start the search'),
            'word.required' => __('Please enter at least 3 characters to start the search'),
        ];
    }
}
