<?php


namespace App\Database\Schema\Grammar;


use Illuminate\Database\Schema\Grammars\PostgresGrammar as IlluminatePostgresGrammar;
use Illuminate\Support\Fluent;

class PostgresGrammar extends IlluminatePostgresGrammar
{
    /**
     * Создать столбец с типом tsvector
     *
     * @param Fluent $column
     * @return string
     */
    protected function typeTsvector(Fluent $column)
    {
        return 'tsvector';
    }
}
