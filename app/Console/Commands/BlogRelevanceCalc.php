<?php

namespace App\Console\Commands;

use App\Models\Blog;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostView;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

use JetBrains\PhpStorm\Deprecated;

class BlogRelevanceCalc extends Command
{
    const ALL_TAG = 'all';
    const LAST_HOUR_TAG = 'lastHour';
    const LAST_DAY_TAG = 'lastDay';

    const POSTS_TAG = 'posts';
    const LIKES_TAG = 'likes';
    const VIEWS_TAG = 'views';

    const BLOG_RELEVANCE_MULTIPLIER = [
        self::ALL_TAG => [
            self::POSTS_TAG => 0.1,
            self::LIKES_TAG => 0.1,
            self::VIEWS_TAG => 0.05,
        ],
        self::LAST_HOUR_TAG => [
            self::LIKES_TAG => 1,
            self::VIEWS_TAG => 0.5,
        ],
        self::LAST_DAY_TAG => [
            self::LIKES_TAG => 0.5,
            self::VIEWS_TAG => 0.25,
        ],
    ];

    const POST_RELEVANCE_MULTIPLIER = [
        self::ALL_TAG => [
            self::LIKES_TAG => 0.1,
            self::VIEWS_TAG => 0.05,
        ],
        self::LAST_HOUR_TAG => [
            self::LIKES_TAG => 1,
            self::VIEWS_TAG => 0.5,
        ],
        self::LAST_DAY_TAG => [
            self::LIKES_TAG => 0.5,
            self::VIEWS_TAG => 0.25,
        ],
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blog:relevance-calc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Blogs and Posts relevance calculation';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        Log::info('Blogs and Posts relevance calculation started!');

        $this->blogsRelevanceCalcV2();
        $this->postsRelevanceCalcV2();

        Log::info('Blogs and Posts relevance calculation finished!');
    }

    /**
     * Расчет релевантности блогов
     */
    #[Deprecated("use BlogRelevanceCalc::blogsRelevanceCalcV2() instead")]
    protected function blogsRelevanceCount()
    {
        $commonBlogsQuery = Blog::select([
                'blogs.id as id',
                \DB::raw('COUNT(posts.id) as posts'),
                \DB::raw('COUNT(likes.user_id) as likes'),
                \DB::raw('COUNT(views.ip) as views'),
            ])
            ->join('posts', 'posts.blog_id', '=', 'blogs.id')
            ->join('post_likes as likes', 'likes.post_id', '=', 'posts.id', 'left')
            ->join('post_views as views', 'views.post_id', '=', 'posts.id', 'left')
            ->groupBy('id');

        $lastHour = now()->addHours(-1);
        $lastHourBlogsQuery = clone $commonBlogsQuery;
        $lastHourBlogsQuery
            ->where('likes.created_at', '>=', $lastHour->format('Y-m-d H:i:s'))
            ->where('views.created_at', '>=', $lastHour->format('Y-m-d H:i:s'));

        $lastDay = now()->addDays(-1);
        $lastDayBlogsQuery = clone $commonBlogsQuery;
        $lastDayBlogsQuery
            ->where('likes.created_at', '>=', $lastDay->format('Y-m-d H:i:s'))
            ->where('views.created_at', '>=', $lastDay->format('Y-m-d H:i:s'));


        $allBlogs = $commonBlogsQuery->get();
        $lastHourBlogs = $lastHourBlogsQuery->get();
        $lastDayBlogs = $lastDayBlogsQuery->get();

        if ($allBlogs->count() == 0) {
            return;
        }

        $maxValues = [
            'all' => [
                'posts' => max(array_column($allBlogs->toArray(), 'posts')),
                'likes' => max(array_column($allBlogs->toArray(), 'likes')),
                'views' => max(array_column($allBlogs->toArray(), 'views')),
            ],
        ];

        if ($lastHourBlogs->count() > 0) {
            $maxValues['lastHour'] = [
                'likes' => max(array_column($lastHourBlogs->toArray(), 'likes')),
                'views' => max(array_column($lastHourBlogs->toArray(), 'views')),
            ];
        }

        if ($lastDayBlogs->count() > 0) {
            $maxValues['lastDay'] = [
                'likes' => max(array_column($lastDayBlogs->toArray(), 'likes')),
                'views' => max(array_column($lastDayBlogs->toArray(), 'views')),
            ];
        }

        foreach ($allBlogs as $blog) {
            $lastHourBlog = $lastHourBlogs->find($blog->id);
            $lastDayBlog = $lastDayBlogs->find($blog->id);

            $relevance =
                $blog->posts / $maxValues['all']['posts'] * 0.1 +
                $blog->likes / $maxValues['all']['likes'] * 0.1 +
                $blog->views / $maxValues['all']['views'] * 0.05;

            if (!empty($lastHourBlog)) {
                $relevance +=
                    $lastHourBlog->likes / $maxValues['lastHour']['likes'] +
                    $lastHourBlog->views / $maxValues['lastHour']['views'] * 0.5;
            }

            if (!empty($lastDayBlog)) {
                $relevance +=
                    $lastDayBlog->likes / $maxValues['lastDay']['likes'] * 0.5 +
                    $lastDayBlog->views / $maxValues['lastDay']['views'] * 0.25;
            }

            $blog->relevance = $relevance;
            $blog->timestamps = false;
            $blog->save();
        }
    }

    /**
     * Расчет релевантности постов
     */
    #[Deprecated("use BlogRelevanceCalc::postsRelevanceCalcV2() instead")]
    protected function postsRelevanceCount()
    {
        $postQuery = Post::select([
                'posts.id as id',
                \DB::raw('COUNT(likes.user_id) as likes'),
                \DB::raw('COUNT(views.ip) as views'),
            ])
            ->join('post_likes as likes', 'likes.post_id', '=', 'posts.id', 'left')
            ->join('post_views as views', 'views.post_id', '=', 'posts.id', 'left')
            ->groupBy('id');

        $lastHour = now()->addHours(-1);
        $lastHourPostsQuery = clone $postQuery;
        $lastHourPostsQuery
            ->where('likes.created_at', '>=', $lastHour->format('Y-m-d H:i:s'))
            ->where('views.created_at', '>=', $lastHour->format('Y-m-d H:i:s'));

        $lastDay = now()->addDays(-1);
        $lastDayPostsQuery = clone $postQuery;
        $lastDayPostsQuery
            ->where('likes.created_at', '>=', $lastDay->format('Y-m-d H:i:s'))
            ->where('views.created_at', '>=', $lastDay->format('Y-m-d H:i:s'));

        $allPosts = $postQuery->get();
        $lastHourPosts = $lastHourPostsQuery->get();
        $lastDayPosts = $lastDayPostsQuery->get();

        if ($allPosts->count() == 0) {
            return;
        }

        $maxValues = [
            'all' => [
                'likes' => max(array_column($allPosts->toArray(), 'likes')),
                'views' => max(array_column($allPosts->toArray(), 'views')),
            ],
        ];

        if ($lastHourPosts->count() > 0) {
            $maxValues['lastHour'] = [
                'likes' => max(array_column($lastHourPosts->toArray(), 'likes')),
                'views' => max(array_column($lastHourPosts->toArray(), 'views')),
            ];
        }

        if ($lastDayPosts->count() > 0) {
            $maxValues['lastDay'] = [
                'likes' => max(array_column($lastDayPosts->toArray(), 'likes')),
                'views' => max(array_column($lastDayPosts->toArray(), 'views')),
            ];
        }

        foreach ($allPosts as $post) {
            $lastHourPost = $lastHourPosts->find($post->id);
            $lastDayPost = $lastDayPosts->find($post->id);

            $relevance =
                $post->likes / $maxValues['all']['likes'] * 0.1 +
                $post->views / $maxValues['all']['views'] * 0.05;

            if (!empty($lastHourPost)) {
                $relevance +=
                    $lastHourPost->likes / $maxValues['lastHour']['likes'] +
                    $lastHourPost->views / $maxValues['lastHour']['views'] * 0.5;
            }

            if (!empty($lastDayPost)) {
                $relevance +=
                    $lastDayPost->likes / $maxValues['lastDay']['likes'] * 0.5 +
                    $lastDayPost->views / $maxValues['lastDay']['views'] * 0.25;
            }

            $post->relevance = $relevance;
            $post->timestamps = false;
            $post->save();
        }
    }

    /**
     * Расчет релевантности блогов V2
     * @see BlogRelevanceCalc::blogsRelevanceCount() Прошлая версия
     */
    protected function blogsRelevanceCalcV2()
    {
        $maxValues = $this->getBlogsMaxValues();

        $commonBlogsQuery = Blog::relevanceCalc();

        $blogs = $commonBlogsQuery->lazyById(100);

        foreach ($blogs as $blog) {

            $relevanceMap = [
                'posts_count' => [
                    'time' => self::ALL_TAG,
                    'type' => self::POSTS_TAG,
                ],
                'likes_count' => [
                    'time' => self::ALL_TAG,
                    'type' => self::LIKES_TAG,
                ],
                'views_count' => [
                    'time' => self::ALL_TAG,
                    'type' => self::VIEWS_TAG,
                ],
                'last_hour_likes_count' => [
                    'time' => self::LAST_HOUR_TAG,
                    'type' => self::LIKES_TAG,
                ],
                'last_hour_views_count' => [
                    'time' => self::LAST_HOUR_TAG,
                    'type' => self::VIEWS_TAG,
                ],
                'last_day_likes_count' => [
                    'time' => self::LAST_DAY_TAG,
                    'type' => self::LIKES_TAG,
                ],
                'last_day_views_count' => [
                    'time' => self::LAST_DAY_TAG,
                    'type' => self::VIEWS_TAG,
                ],
            ];

            $relevance = 0;
            foreach ($relevanceMap as $attrName => $settings) {
                if ($blog->$attrName > 0) {
                    $relevance +=
                        $blog->$attrName / $maxValues[$settings['time']][$settings['type']] * self::BLOG_RELEVANCE_MULTIPLIER[$settings['time']][$settings['type']];
                }
            }

            $blog->relevance = $relevance;
            $blog->timestamps = false;
            $blog->save();
        }
    }

    /**
     * Расчет максимальных значений для расчета релевантности блогов
     */
    protected function getBlogsMaxValues(): array
    {
        $postsMaxCountQuery = Post::whereNotNull('blog_id')->groupBy(['blog_id'])->orderByRaw('COUNT("id") DESC')->selectRaw('COUNT("id")');

        $likesMaxCountQuery = PostLike::countPerBlog()
            ->orderByRaw('likes DESC');

        $viewsMaxCountQuery = PostView::countPerBlog()
            ->orderByRaw('views DESC');

        $lastHour = now()->addHours(-1);
        $lastDay = now()->addDays(-1);

        return [
            self::ALL_TAG => [
                self::POSTS_TAG => $postsMaxCountQuery->first()?->count ?? 0,
                self::LIKES_TAG => $likesMaxCountQuery->first()?->likes ?? 0,
                self::VIEWS_TAG => $viewsMaxCountQuery->first()?->views ?? 0,
            ],
            self::LAST_HOUR_TAG => [
                self::LIKES_TAG => (clone $likesMaxCountQuery)->where('post_likes.created_at', '>=', $lastHour->format('Y-m-d H:i:s'))->first()?->likes ?? 0,
                self::VIEWS_TAG => (clone $viewsMaxCountQuery)->where('post_views.created_at', '>=', $lastHour->format('Y-m-d H:i:s'))->first()?->views ?? 0,
            ],
            self::LAST_DAY_TAG => [
                self::LIKES_TAG => (clone $likesMaxCountQuery)->where('post_likes.created_at', '>=', $lastDay->format('Y-m-d H:i:s'))->first()?->likes ?? 0,
                self::VIEWS_TAG => (clone $viewsMaxCountQuery)->where('post_views.created_at', '>=', $lastDay->format('Y-m-d H:i:s'))->first()?->views ?? 0,
            ],
        ];
    }

    /**
     * Расчет релевантности постов
     * @see BlogRelevanceCalc::postsRelevanceCount() Прошлая версия
     */
    protected function postsRelevanceCalcV2()
    {
        $maxValues = $this->getPostsMaxValues();

        $postsQuery = Post::relevanceCalc();

        $posts = $postsQuery->lazyById(100);

        $relevances = [];
        foreach ($posts as $post) {
            $relevanceMap = [
                'likes_count' => [
                    'time' => self::ALL_TAG,
                    'type' => self::LIKES_TAG,
                ],
                'views_count' => [
                    'time' => self::ALL_TAG,
                    'type' => self::VIEWS_TAG,
                ],
                'last_hour_likes_count' => [
                    'time' => self::LAST_HOUR_TAG,
                    'type' => self::LIKES_TAG,
                ],
                'last_hour_views_count' => [
                    'time' => self::LAST_HOUR_TAG,
                    'type' => self::VIEWS_TAG,
                ],
                'last_day_likes_count' => [
                    'time' => self::LAST_DAY_TAG,
                    'type' => self::LIKES_TAG,
                ],
                'last_day_views_count' => [
                    'time' => self::LAST_DAY_TAG,
                    'type' => self::VIEWS_TAG,
                ],
            ];

            $relevance = 0;
            foreach ($relevanceMap as $attrName => $settings) {
                if ($post->$attrName > 0) {
                    $relevance +=
                        $post->$attrName / $maxValues[$settings['time']][$settings['type']] * self::POST_RELEVANCE_MULTIPLIER[$settings['time']][$settings['type']];
                }
            }

            $post->relevance = $relevance;
            $post->timestamps = false;
            $post->save();
        }
    }

    /**
     * Расчет максимальных значений для расчета релевантности постов
     */
    protected function getPostsMaxValues(): array
    {
        $likesMaxCountQuery = PostLike::countPerPost()
            ->orderByRaw('likes DESC');
        $viewsMaxCountQuery = PostView::countPerPost()
            ->orderByRaw('views DESC');

        $lastHour = now()->addHours(-1);
        $lastDay = now()->addDays(-1);

        return [
            self::ALL_TAG => [
                self::LIKES_TAG => $likesMaxCountQuery->first()?->likes ?? 0,
                self::VIEWS_TAG => $viewsMaxCountQuery->first()?->views ?? 0,
            ],
            self::LAST_HOUR_TAG => [
                self::LIKES_TAG => (clone $likesMaxCountQuery)->where('created_at', '>=', $lastHour->format('Y-m-d H:i:s'))->first()?->likes ?? 0,
                self::VIEWS_TAG => (clone $viewsMaxCountQuery)->where('created_at', '>=', $lastHour->format('Y-m-d H:i:s'))->first()?->views ?? 0,
            ],
            self::LAST_DAY_TAG => [
                self::LIKES_TAG => (clone $likesMaxCountQuery)->where('created_at', '>=', $lastDay->format('Y-m-d H:i:s'))->first()?->likes ?? 0,
                self::VIEWS_TAG => (clone $viewsMaxCountQuery)->where('created_at', '>=', $lastDay->format('Y-m-d H:i:s'))->first()?->views ?? 0,
            ],
        ];
    }
}
