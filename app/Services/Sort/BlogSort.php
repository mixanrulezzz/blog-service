<?php


namespace App\Services\Sort;


class BlogSort extends QuerySort
{
    public function sort(): void
    {
        $sort = $this->request->get('sort', 'published_at');
        $side = $this->request->get('side', 'desc');

        match ($sort) {
            'title' => $this->builder->orderBy('blogs.title', $side),
            'relevance' => $this->builder->orderBy('blogs.relevance', $side),
            default => $this->builder->orderBy('blogs.updated_at', $side),
        };
    }

    public static function getSortList(): array
    {
        return [
            'title',
            'published_at',
            'relevance',
        ];
    }
}
