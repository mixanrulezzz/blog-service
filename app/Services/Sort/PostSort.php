<?php


namespace App\Services\Sort;


use Illuminate\Database\Eloquent\Builder;

class PostSort extends QuerySort
{
    function sort(): void
    {
        $sort = $this->request->get('sort', 'published_at');
        $side = $this->request->get('side', 'desc');

        match ($sort) {
            'title' => $this->builder->orderBy('posts.title', $side),
            'likes' => $this->builder->withCount('likes')
                ->orderBy('likes_count', $side),
            'relevance' => $this->builder->orderBy('posts.relevance', $side),
            default => $this->builder->orderBy('posts.published_at', $side),
        };
    }

    public static function getSortList(): array
    {
        return [
            'title',
            'published_at',
            'likes',
            'relevance',
        ];
    }
}
