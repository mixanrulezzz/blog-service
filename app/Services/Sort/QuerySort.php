<?php


namespace App\Services\Sort;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

abstract class QuerySort
{
    protected Builder $builder;

    public function __construct(
        public Request $request
    )
    {
    }

    public function apply(Builder $builder): Builder
    {
        $this->builder = $builder;

        $this->sort();

        return $this->builder;
    }

    abstract public function sort(): void;

    abstract public static function getSortList(): array;
}
