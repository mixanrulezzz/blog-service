<?php


namespace App\Services\Filters\Traits;


use Illuminate\Database\Eloquent\Builder;

trait DateFilterTrait
{
    protected Builder $builder;

    /**
     * Получить колонку, которую нужно отфильтровать по дате
     * @return string
     */
    abstract function getDateFilterColumn(): string;

    /**
     * Фильтрация по минимальной дате публикации поста
     * @param string $value
     * @return Builder
     */
    public function dateFromParamFilter(string $value): Builder
    {
        $dateFrom = \Date::createFromFormat('Y-m-d', $value);

        return $this->builder->whereDate($this->getDateFilterColumn(), '>=', $dateFrom);
    }

    /**
     * Фильтрация по максимальной дате публикации поста
     * @param string $value
     * @return Builder
     */
    public function dateToParamFilter(string $value): Builder
    {
        $dateTo = \Date::createFromFormat('Y-m-d', $value);

        return $this->builder->whereDate($this->getDateFilterColumn(), '<=', $dateTo);
    }
}
