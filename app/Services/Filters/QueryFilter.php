<?php


namespace App\Services\Filters;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

abstract class QueryFilter
{
    protected Builder $builder;
    protected string $delimeter = ',';
    protected string $mainFilterTable;

    /**
     * QueryFilter constructor.
     * @param Request $request
     */
    public function __construct(
        public Request $request
    )
    {
    }

    /**
     * Получение значения фильтров из request
     * @return string|array|null
     */
    public function filters(): string|array|null
    {
        return $this->request->query();
    }

    /**
     * Фильтрация
     * @param Builder $builder
     * @return Builder
     */
    public function apply(Builder $builder): Builder
    {
        $this->builder = $builder;

        foreach ($this->filters() as $name => $value) {
            $funcValues = array_filter($this->paramToArray($value));

            if (!empty($funcValues) && method_exists($this, $this->prepareFilterFuncName($name))) {
                call_user_func_array([$this, $this->prepareFilterFuncName($name)], $funcValues);
            }
        }

        return $this->builder;
    }

    /**
     * @param $param
     * @return array
     */
    protected function paramToArray($param): array
    {
        return explode($this->delimeter, $param);
    }

    /**
     * Подготовка имени для функции фильтрации отдельного параметра
     * @param string $name
     * @return string
     */
    protected function prepareFilterFuncName(string $name): string
    {
        return Str::camel($name) . 'ParamFilter';
    }
}
