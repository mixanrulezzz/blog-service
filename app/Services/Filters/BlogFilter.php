<?php


namespace App\Services\Filters;


use App\Services\Filters\Traits\DateFilterTrait;
use Illuminate\Database\Eloquent\Builder;

class BlogFilter extends QueryFilter
{
    use DateFilterTrait;

    /**
     * Полнотекстовый поиск по заголовку поста или имени автора поста
     * @param string $value
     * @return Builder
     */
    public function wordParamFilter(string $value): Builder
    {
        return $this->builder
            ->addSelect([
                \DB::raw(
                    'sum(ts_rank(title_tsvector.title_tsvector, plainto_tsquery(\'' . $value . '\'))) + sum(ts_rank(owner_name_tsvector.name_vector, plainto_tsquery(\'' . $value . '\'))) + sum(ts_rank(authors_name_tsvector.name_vector, plainto_tsquery(\'' . $value . '\'))) as full_tsrank'
                ),
            ])
            ->join('users as owner', 'blogs.owner_id', '=', 'owner.id')

            ->join('blog_authors', 'blogs.id', '=', 'blog_authors.blog_id')
            ->join('users as authors', 'blog_authors.author_id', '=', 'authors.id')
            ->join('users_name_tsvector as authors_name_tsvector', 'authors.id', '=', 'authors_name_tsvector.id')

            ->join('blogs_title_tsvector as title_tsvector', 'blogs.id', '=', 'title_tsvector.id')
            ->join('users_name_tsvector as owner_name_tsvector', 'owner.id', '=', 'owner_name_tsvector.id')
            ->where(
                fn(Builder $query) =>
                    $query
                        ->where('title_tsvector.title_tsvector', '@@', \DB::raw('plainto_tsquery(\'' . $value . '\')'))
                        ->orWhere('owner_name_tsvector.name_vector', '@@', \DB::raw('plainto_tsquery(\'' . $value . '\')'))
                        ->orWhere('authors_name_tsvector.name_vector', '@@', \DB::raw('plainto_tsquery(\'' . $value . '\')'))
            )
            ->orderByDesc('full_tsrank')
            ->groupBy(['blogs.id']);
    }

    /**
     * @inheritDoc
     * @return string
     */
    public function getDateFilterColumn(): string
    {
        return 'blogs.created_at';
    }
}
