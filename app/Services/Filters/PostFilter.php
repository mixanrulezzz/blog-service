<?php


namespace App\Services\Filters;


use App\Services\Filters\Traits\DateFilterTrait;
use Illuminate\Database\Eloquent\Builder;

class PostFilter extends QueryFilter
{
    use DateFilterTrait;

    /**
     * Полнотекстовый поиск по заголовку поста или имени автора поста
     * @param string $value
     * @return Builder
     */
    public function wordParamFilter(string $value): Builder
    {
        return $this->builder
            ->addSelect([
                \DB::raw(
                    'ts_rank(title_tsvector.title_tsvector, plainto_tsquery(\'' . $value . '\')) + ts_rank(name_tsvector.name_vector, plainto_tsquery(\'' . $value . '\')) as full_tsrank'
                ),
            ])
            ->join('users as owner', 'posts.owner_id', '=', 'owner.id')
            ->join('posts_title_tsvector as title_tsvector', 'posts.id', '=', 'title_tsvector.id')
            ->join('users_name_tsvector as name_tsvector', 'owner.id', '=', 'name_tsvector.id')
            ->where(
                fn(Builder $query) =>
                    $query
                        ->where('name_tsvector.name_vector', '@@', \DB::raw('plainto_tsquery(\'' . $value . '\')'))
                        ->orWhere('title_tsvector.title_tsvector', '@@', \DB::raw('plainto_tsquery(\'' . $value . '\')'))
            )
            ->orderByDesc('full_tsrank');
    }

    /**
     * @inheritDoc
     * @return string
     */
    public function getDateFilterColumn(): string
    {
        return 'posts.published_at';
    }
}
