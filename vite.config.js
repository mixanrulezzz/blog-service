import { defineConfig } from 'vite';
import laravel, { refreshPaths } from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/css/app.css',
                'resources/js/app.js',
                'resources/js/post-edit.js',
                'resources/js/blog-edit.js',
                'resources/js/blog-subscribe.js',
                'resources/js/post-detail.js',
                'resources/js/main.js',
                'resources/js/blog-list.js',
            ],
            refresh: [
                ...refreshPaths,
                'app/Http/Livewire/**',
            ],
        }),
    ],
});
