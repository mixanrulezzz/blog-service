<?php

use App\Http\Controllers\AuthorsController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserBlogController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [PostController::class, 'list'])
    ->name('main');

Route::controller(PostController::class)->name('post.')
    ->group(function () {
        Route::get('/post/{post}', 'detail')
            ->name('detail')
            ->scopeBindings();
        Route::post('/post/view', 'view')
            ->name('view');
        Route::post('/post/like', 'like')
            ->name('like');
});

Route::controller(BlogController::class)->name('blog.')
    ->group(function () {
        Route::get('/blogs', 'list')
            ->name('list');
        Route::get('/blog/{blog}', 'detail')
            ->name('detail');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::controller(UserBlogController::class)->name('user.')
        ->group(function () {
            Route::get('/my-posts', 'userPosts')
                ->name('posts');
            Route::get('/my-blogs', 'userBlogs')
                ->name('blogs');
            Route::get('/blogs/subscription', 'userSubscriptions')
                ->name('subscriptions');
    });

    Route::controller(BlogController::class)->name('blog.')
        ->group(function () {
            Route::get('/blog/new', 'edit')
                ->name('new');
            Route::get('/blog/edit/{blog}', 'edit')
                ->name('edit');
            Route::post('/blog', 'save')
                ->name('create');
            Route::put('/blog/{blog}', 'save')
                ->name('save');
            Route::delete('/blog/{blog}', 'delete')
                ->name('delete');

            Route::post('/blog/subscribe/{blog}', 'subscribe')
                ->name('subscribe');
    });

    Route::controller(PostController::class)->name('post.')
        ->group(function () {
            Route::get('/post/new', 'edit')
                ->name('new');
            Route::get('/post/edit/{post}', 'edit')
                ->name('edit');
            Route::post('/post', 'save')
                ->name('create');
            Route::put('/post/{post}', 'save')
                ->name('save');
            Route::delete('/post/{post}', 'delete')
                ->name('delete');
    });

    Route::controller(CommentController::class)->name('comment.')
        ->group(function () {
            Route::post('/comment', 'save')
                ->name('create');
            Route::put('/comment/{comment}', 'save')
                ->name('save');
            Route::delete('/comment/{comment}', 'delete')
                ->name('delete');
    });

    Route::post('/authors/search', [AuthorsController::class, 'search'])
        ->name('authors.search');
});
