<?php

declare(strict_types=1);

return [
    'relevance' => 'By relevance',
    'published_at' => 'By published at',
    'likes' => 'By likes',
    'title' => 'By title',
];
