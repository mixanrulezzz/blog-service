<?php

declare(strict_types=1);

return [
    'relevance' => 'По актуальности',
    'published_at' => 'По дате обновления',
    'likes' => 'По лайкам',
    'title' => 'По названию',
];
