import FilterAndSortForm from "./classes/filter-and-sort-form";

document.addEventListener("DOMContentLoaded", () => {
    const form = new FilterAndSortForm({
        form: document.querySelector('form#search-blogs'),
    });
});
