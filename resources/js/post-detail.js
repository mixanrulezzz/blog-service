import {Csrf} from "./classes/csrf";
import Form from "./classes/form";
import DeleteButton from "./classes/delete-button";

document.addEventListener("DOMContentLoaded", () => {
    const postId = document.querySelector('.js-post-container').dataset.postId;

    if (postId) {
        fetch('/post/view', {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                post_id: postId,
                _token: Csrf.getToken(),
            }),
        }).then((response) => {
            if (!response.ok) {
                return response.text().then(text => { throw new Error(text) })
            }

            return response.json();
        }).then((data) => {
            document.querySelectorAll('.js-post-views-count').forEach((elem) => {
                elem.textContent = data.views;
            });
        }).catch((err) => {
            console.log(err.message);
        });

        document.querySelectorAll('.js-post-like-button').forEach((elem) => {
            elem.addEventListener('click', (evt) => {
                const isLiked = elem.classList.contains('liked');

                fetch('/post/like', {
                    method: 'POST',
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        post_id: postId,
                        action: isLiked ? 'delete' : 'add',
                        _token: Csrf.getToken(),
                    }),
                }).then((response) => {
                    if (!response.ok) {
                        return response.text().then(text => { throw new Error(text) })
                    }

                    return response.json();
                }).then((data) => {
                    document.querySelectorAll('.js-post-likes-count').forEach((countContainer) => {
                        countContainer.textContent = data.likes;

                        document.querySelectorAll('.js-post-like-button').forEach((btn) => {
                            if (isLiked) {
                                btn.classList.remove('liked');
                            } else {
                                btn.classList.add('liked');
                            }
                        });
                    });
                }).catch((err) => {
                    console.log(err.message);
                });
            })
        });
    }

    const commentEditFormContainer = document.querySelector('#edit-comment-form');
    const commentEditForm = new Form({
        form: commentEditFormContainer,
        submitHandler: (evt) => {
            const formData = commentEditForm.getCurFormData();
            const dataToSend = Object.fromEntries(formData.entries());

            let editUrl = '/comment';
            let method = 'POST';
            if (formData.get('id')) {
                editUrl += '/' + formData.get('id');
                method = 'PUT';
            }

            commentEditForm
                .sendData({
                    url: editUrl,
                    data: dataToSend,
                    method: method,
                })
                .then((response) => {
                    document.location.reload();
                })
                .catch((err) => {
                    try {
                        const errors = JSON.parse(err.message);
                        Object.keys(errors).forEach((inputName) => {
                            commentEditForm.showInputError({
                                inputName: inputName,
                                errorMessage: errors[inputName][0],
                            });
                        })
                    } catch (e) {
                        console.log(err.message);
                    }
                });

        },
    });

    document.querySelectorAll('.js-add-comment').forEach((btn) => {
        btn.addEventListener('click', () => {
            commentEditFormContainer.querySelector('.js-header-for-add').classList.remove('hidden');
            commentEditFormContainer.querySelector('.js-header-for-edit').classList.add('hidden');
            commentEditFormContainer.querySelector('.js-add-button').classList.remove('hidden');
            commentEditFormContainer.querySelector('.js-edit-button').classList.add('hidden');

            const postId = btn.dataset.postId;

            commentEditFormContainer.querySelector('input[name="id"]').value = null;
            commentEditFormContainer.querySelector('input[name="post-id"]').value = postId;
            commentEditFormContainer.querySelector('textarea[name="body"]').textContent = null;
        })
    });

    document.querySelectorAll('.js-edit-comment').forEach((btn) => {
        btn.addEventListener('click', () => {
            commentEditFormContainer.querySelector('.js-header-for-add').classList.add('hidden');
            commentEditFormContainer.querySelector('.js-header-for-edit').classList.remove('hidden');
            commentEditFormContainer.querySelector('.js-add-button').classList.add('hidden');
            commentEditFormContainer.querySelector('.js-edit-button').classList.remove('hidden');

            const commentId = btn.dataset.commentId;
            const postId = btn.dataset.postId;
            const commentBody = btn.closest('.js-comment-one').querySelector('.js-comment-body').textContent;

            commentEditFormContainer.querySelector('input[name="id"]').value = commentId;
            commentEditFormContainer.querySelector('input[name="post-id"]').value = postId;
            commentEditFormContainer.querySelector('textarea[name="body"]').textContent = commentBody;
        })
    });
});
