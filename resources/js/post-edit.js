import EditorJS from '@editorjs/editorjs';
import Header from '@editorjs/header';
import LinkTool from '@editorjs/link';
import RawTool from '@editorjs/raw';
import SimpleImage from '@editorjs/simple-image';
import NestedList from '@editorjs/nested-list';
import Form from "./classes/form";

document.addEventListener("DOMContentLoaded", () => {
    const editorContainer = document.querySelector('#editorjs');
    let editorData = {};

    try {
        editorData = JSON.parse(editorContainer.dataset.data);
    } catch (e) {}

    const editor = new EditorJS({
        /**
         * Id of Element that should contain Editor instance
         */
        holder: 'editorjs',

        /**
         * Available Tools list.
         * Pass Tool's class or Settings object for each Tool you want to use
         */
        tools: {
            header: {
                class: Header,
                config: {
                    placeholder: 'Enter a header',
                    levels: [2, 3, 4],
                    defaultLevel: 3
                }
            },
            link: LinkTool,
            raw: RawTool,
            image: SimpleImage,
            list: {
                class: NestedList,
                inlineToolbar: true,
                config: {
                    defaultStyle: 'unordered'
                },
            },
        },

        data: editorData,
    });

    const postForm = document.getElementById('editPostForm');

    const editPostFormObj = new Form({
        form: postForm,
        submitHandler: (evt) => {
            editPostFormObj.hideAllInputErrors();

            editor.save().then((outputData) => {
                const formData = editPostFormObj.getCurFormData();
                formData.append('body', JSON.stringify(outputData));
                formData.append('is_published', editPostFormObj.getInputByName('is_published').checked);
                let editUrl = '/post';
                let method = 'POST';

                if (formData.get('id')) {
                    editUrl += '/' + formData.get('id');
                    method = 'PUT';
                }

                editPostFormObj
                    .sendData({
                        url: editUrl,
                        data: Object.fromEntries(formData.entries()),
                        method: method,
                    })
                    .then((response) => {
                        document.location.href = response.url;
                    })
                    .catch((err) => {
                        try {
                            const errors = JSON.parse(err.message);
                            Object.keys(errors).forEach((inputName) => {
                                editPostFormObj.showInputError({
                                    inputName: inputName,
                                    errorMessage: errors[inputName][0],
                                });
                            })
                        } catch (e) {
                            console.log(err.message);
                        }
                    });
            }).catch((error) => {
                console.log('Saving failed: ', error)
            });
        }
    });
});


