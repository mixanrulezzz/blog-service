import {Csrf} from "./csrf";

export default class BlogSubscription {
    constructor() {
        this.setEventListeners();
    }

    setEventListeners() {
        document.querySelectorAll('.js-blog-subscribe-btn').forEach((elem) => {
            elem.addEventListener('click', (evt) => {
                this.subscriptionBtnsClickHandler(elem, 'subscribe');
            });
        });

        document.querySelectorAll('.js-blog-unsubscribe-btn').forEach((elem) => {
            elem.addEventListener('click', (evt) => {
                this.subscriptionBtnsClickHandler(elem, 'unsubscribe');
            });
        });
    }

    subscriptionBtnsClickHandler(elem, action) {
        elem.setAttribute('disabled', 'disabled');
        const container = elem.closest('.js-blog-subscription-container');
        const blogId = container.dataset.blogId;

        this.send(action, blogId).then(() => {
            switch (action) {
                case 'subscribe':
                    container.classList.add(['unsubscribe']);
                    container.classList.remove(['subscribe']);
                    break;
                case 'unsubscribe':
                    container.classList.add(['subscribe']);
                    container.classList.remove(['unsubscribe']);
                    break;
            }

            elem.removeAttribute('disabled');
        });
    }

    send(action, blogId) {
        return fetch('/blog/subscribe/' + blogId, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                action: action,
                _token: Csrf.getToken(),
            }),
        }).then((response) => {
            if (!response.ok) {
                return response.text().then(text => { throw new Error(text) })
            }

            return response.text();
        });
    }
}
