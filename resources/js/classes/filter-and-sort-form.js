import {Dropdown} from "flowbite";
import DateFilter from "./date-filter";

export default class FilterAndSortForm {
    constructor({form}) {
        this.form = form;
        this.searchInput = this.form.querySelector('input[name="word"]');
        this.dateFromInput = this.form.querySelector('input[name="date-from"]');
        this.dateToInput = this.form.querySelector('input[name="date-to"]');
        this._init();
    }

    _init() {
        const filterMenu = this.form.querySelector('.js-filter-dropdown-menu');
        const filterButton = this.form.querySelector('.js-filter-dropdown-btn');

        const filterDropdown = new Dropdown(filterMenu, filterButton, {
            placement: 'bottom',
            triggerType: 'click',
            offsetSkidding: 44,
        });

        const sortMenu = this.form.querySelector('.js-sort-dropdown-menu');
        const sortButton = this.form.querySelector('.js-sort-dropdown-btn');

        const sortDropdown = new Dropdown(sortMenu, sortButton, {
            placement: 'bottom',
            triggerType: 'click',
            offsetSkidding: 0,
        });

        const dateFilter = new DateFilter({
            fromDateInput: this.dateFromInput,
            toDateInput: this.dateToInput,
        });

        const sideMenu = this.form.querySelector('.js-side-dropdown-menu');

        this.form.querySelectorAll('.js-sort-item').forEach((sortItem) => {
            const sideDropdownMenu = sortItem.querySelector('.js-side-dropdown-menu');
            const sideDropdownBtn = sortItem.querySelector('.js-side-dropdown-btn');

            const sideDropdown = new Dropdown(sideDropdownMenu, sideDropdownBtn, {
                placement: 'right',
                triggerType: 'hover',
                offsetDistance: -10,
            });
        });

        this._setEventListeners();
    }

    _setEventListeners() {
        this.searchInput.addEventListener('keydown', (evt) => {
            if (evt.keyCode === 13) {
                this.form.dispatchEvent(new Event('submit'));
            }
        });

        this.form.addEventListener('formdata', (evt) => {
            let formData = evt.formData;
            for (let [name, value] of Array.from(formData.entries())) {
                if (value === '') formData.delete(name);
            }
        });

        this.form.querySelectorAll('.js-filter-reset')
            .forEach((elem) => {
                elem.addEventListener('click', (evt) => {
                    window.location = window.location.href.split("?")[0];
                });
            });

        this.form.querySelectorAll('.js-side-item')
            .forEach((sideItem) => {
                const sortValue = sideItem.closest('.js-sort-item').dataset.value;

                sideItem.addEventListener('click', (evt) => {
                    const url = new URL(window.location.href);
                    url.searchParams.set('sort', sortValue);
                    url.searchParams.set('side', sideItem.dataset.value);

                    window.location = url.href;
                });
            });
    }
}
