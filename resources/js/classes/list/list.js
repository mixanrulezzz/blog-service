export default class List {
    constructor({itemTemplate, listContainer}) {
        this.itemTemplate = itemTemplate;
        this.listContainer = listContainer;
    }

    addToList(data) {
        const newItem = this.itemTemplate.content.cloneNode(true);

        this._setItemContent(newItem, data);
        this._setItemListeners(newItem);

        this.listContainer.appendChild(newItem);
    }

    resetList() {
        this.getAllItems().forEach((element) => {
            element.remove();
        });
    }

    _setItemContent(item, data) {
        item.querySelector('.js-item-value').value = data.value;
        item.querySelector('.js-item-text').textContent = data.text;
    }

    _setItemListeners(item) {}

    getItemByValue(value) {
        const valueContainer = this.listContainer.querySelector('.js-item-value[value="' + value + '"]');
        if (valueContainer) {
            return valueContainer.closest('.js-list-item');
        } else {
            return null;
        }
    }

    getAllItems()
    {
        return this.listContainer.querySelectorAll('.js-list-item');
    }
}
