import List from "./list";

export default class ListWithAddButton extends List {
    constructor({itemTemplate, listContainer}) {
        super({itemTemplate: itemTemplate, listContainer: listContainer});
        this.addHandler = () => {};
    }

    addToList({value, text, buttonState}) {
        super.addToList({value: value, text: text, buttonState: buttonState});
    }

    setAddHandler(addHandler) {
        this.addHandler = addHandler;
    }

    changeAddButtonState(item, state)
    {
        const button = item.querySelector('.js-add-button');
        const icon = item.querySelector('.js-icon');

        switch (state) {
            case 'active':
                button.dataset.ui = state;
                icon.dataset.ui = state;
                icon.classList.remove('fa-user-check');
                icon.classList.add('fa-user-plus');
                button.addEventListener('click', this.addHandler);
                break;

            case 'passive':
                button.dataset.ui = state;
                icon.dataset.ui = state;
                icon.classList.remove('fa-user-plus');
                icon.classList.add('fa-user-check');
                button.removeEventListener('click', this.addHandler);
                break;
        }
    }

    _setItemContent(item, data) {
        super._setItemContent(item, data);

        this.changeAddButtonState(item, data.buttonState);
    }
}
