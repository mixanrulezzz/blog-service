import List from "./list";

export default class ListWithDeleteButton extends List{
    constructor({itemTemplate, listContainer}) {
        super({itemTemplate: itemTemplate, listContainer: listContainer});
        this.deleteHandler = () => {};
    }

    addToList({value, text}) {
        super.addToList({value: value, text: text});
    }

    setDeleteHandler(deleteHandler) {
        this.deleteHandler = deleteHandler;
    }

    _setItemListeners(item) {
        item.querySelector('.js-delete-btn').addEventListener('click', this.deleteHandler);
    }
}
