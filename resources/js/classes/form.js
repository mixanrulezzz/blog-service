export default class Form {
    constructor({form, submitHandler}) {
        this.form = form;
        this.submitHandler = submitHandler;
        this.setEventHandler();
    }

    setEventHandler(){
        this.form.addEventListener('submit', (evt) => {
            evt.preventDefault();
            this.submitHandler(evt);
        });

        for (const inputName of this.getAllInputNames()) {
            this.getInputByName(inputName).addEventListener('input', (evt) => {
                this.hideInputError(inputName);
            });
        }
    }

    _getInputContainer(inputName) {
        return this.getInputByName(inputName).closest('.js-input-container');
    }

    getInputByName(inputName) {
        return this.form.querySelector('[name="' + inputName + '"]');
    }

    showInputError({inputName, errorMessage}) {
        const inputContainer = this._getInputContainer(inputName);

        if (inputContainer) {
            inputContainer.querySelector('.js-input-error-message').textContent = errorMessage;
            inputContainer.classList.add('error');
        }
    }

    hideInputError(inputName) {
        const inputContainer = this._getInputContainer(inputName);

        if (inputContainer) {
            inputContainer.querySelector('.js-input-error-message').textContent = '';
            inputContainer.classList.remove('error');
        }
    }

    hideAllInputErrors() {
        for (const inputName of this.getAllInputNames()) {
            this.hideInputError(inputName);
        }
    }

    getAllInputNames() {
        return this.getCurFormData().keys();
    }

    getCurFormData() {
        return new FormData(this.form);
    }

    sendData({url, data, method}) {
        return fetch(url, {
            method: method ?? 'POST',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        })
            .then((response) => {
                if (!response.ok) {
                    return response.text().then(text => { throw new Error(text) })
                }

                return response.json();
            });
    }
}

