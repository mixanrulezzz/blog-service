import {Dropdown} from "flowbite";
import Search from "./search";
import ListWithAddButton from "./list/list-with-add-button";
import ListWithDeleteButton from "./list/list-with-delete-button";

export default class ModalWithMultiAddForm {
    constructor({modalContainer, searchUrl, searchTimeout, submitHandler}) {
        this.modalContainer = modalContainer;
        this.searchUrl = searchUrl;
        this.searchTimeout = searchTimeout ?? 500;
        this.submitHandler = submitHandler;
        this.prepareSearchData = (data) => { return data; };
        this._init();
    }

    _init() {
        this.searchInput = this.modalContainer.querySelector('.js-search-input');

        this._initSearchDropdown();

        this._initLists();

        this._initSearch();

        this._setEventListeners();
    }

    _initSearchDropdown() {
        const dropdownMenu = this.modalContainer.querySelector('.js-search-dropdown');

        this.searchDropdown = new Dropdown(dropdownMenu, this.searchInput, {
            placement: 'bottom',
            triggerType: 'click',
            onHide: () => {
                // Нужно чтобы блок показывался при повторном нажатии на инпут
                if (this.searchInput === document.activeElement) {
                    this.searchDropdown.show();
                }
            },
        });
    }

    _initSearch() {
        this.searchObj = new Search({
            input: this.searchInput,
            url: this.searchUrl,
            method: 'POST',
            timeout: this.searchTimeout,
            searchHandler: (data) => {
                this.getSearchErrorBlock().classList.add('hidden');
                this.searchList.resetList();

                if (data.length > 0) {
                    this.getEmptySearchBlock().classList.add(['hidden']);

                    const dataToAdd = this.prepareSearchData(data);

                    dataToAdd.forEach((item) => {
                        this.searchList.addToList(item)
                    });
                } else {
                    this.getEmptySearchBlock().classList.remove(['hidden']);
                }
            },
            errorHandler: (errors) => {
                this.searchList.resetList();
                this.getEmptySearchBlock().classList.add(['hidden']);

                if (errors.word) {
                    this.getSearchErrorBlock().classList.remove('hidden');
                    this.getSearchErrorBlock().querySelector('.js-error-text').textContent = errors.word[0];
                }
            }
        });
    }

    _initLists() {
        this.searchList = new ListWithAddButton({
            itemTemplate: this.modalContainer.querySelector('.js-search-item-template'),
            listContainer: this.modalContainer.querySelector('.js-search-result-container'),
        });

        this.addList = new ListWithDeleteButton({
            itemTemplate: this.modalContainer.querySelector('.js-to-add-item-template'),
            listContainer: this.modalContainer.querySelector('.js-to-add-list-container'),
        });

        this.searchList.setAddHandler((evt) => {
            const listItem = evt.target.closest('.js-list-item');

            let value = 0;
            if (listItem.classList.contains('js-item-value')) {
                value = listItem.value;
            } else {
                value = listItem.querySelector('.js-item-value').value;
            }

            const text = listItem.querySelector('.js-item-text').textContent;
            this.addList.addToList({
                value: value,
                text: text,
            });
            this.searchList.changeAddButtonState(listItem, 'passive');
        });

        this.addList.setDeleteHandler((evt) => {
            const listItem = evt.target.closest('.js-list-item');

            let value = null;
            if (listItem.classList.contains('js-item-value')) {
                value = listItem.value;
            } else {
                value = listItem.querySelector('.js-item-value').value;
            }

            const searchItem = this.searchList.getItemByValue(value);
            if (searchItem) {
                this.searchList.changeAddButtonState(searchItem,  'active');
            }

            listItem.remove();
        })
    }

    _setEventListeners() {
        const closeModalBtns = document.querySelectorAll('.js-modal-close[data-modal="' + this.getModalId() + '"]');
        closeModalBtns.forEach((btn) => {
            btn.addEventListener('click', () => {
                this.searchInput.value = '';
                this.searchInput.dispatchEvent(new Event('input', {bubbles: true}));
            })
        });

        const submitBtn = document.querySelector('.js-submit-button');
        submitBtn.addEventListener('click', () => {
            this.submitHandler(this.addList.getAllItems());

            this.addList.resetList();
            this.searchInput.value = '';
            this.searchInput.dispatchEvent(new Event('input', { bubbles: true }));
        });

        const resetBtn = document.querySelector('.js-reset-button');
        resetBtn.addEventListener('click', () => {
            this.addList.resetList();
            this.searchInput.value = '';
            this.searchInput.dispatchEvent(new Event('input', { bubbles: true }));
        });
    }

    getModalId() {
        return this.modalContainer.id;
    }

    getEmptySearchBlock() {
        if (!this.emptySearchBlock) {
            this.emptySearchBlock = this.modalContainer.querySelector('.js-empty-search-block');
        }

        return this.emptySearchBlock;
    }

    getSearchErrorBlock() {
        if (!this.searchErrorBlock) {
            this.searchErrorBlock = this.modalContainer.querySelector('.js-search-error-block');
        }

        return this.searchErrorBlock;
    }

    setPrepareSearchDataFunc(prepareSearchDataFunc) {
        this.prepareSearchData = prepareSearchDataFunc;
    }
}
