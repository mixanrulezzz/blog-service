import {Modal} from "flowbite";
import DeleteButton from "./delete-button";

export default class App {
    constructor() {
        this._initModals();

        this._initDeleteButtons();
    }

    _initModals() {
        this.modals = {};

        document.addEventListener("DOMContentLoaded", () => {
            document.querySelectorAll('.js-modal-container').forEach((modalContainer) => {
                if (modalContainer.dataset.modal && !this.modals[modalContainer.dataset.modal]) {
                    this.modals[modalContainer.dataset.modal] = new Modal(modalContainer, {
                        backdrop: 'static',
                    });
                }
            });

            document.querySelectorAll('.js-modal-open').forEach((openBtn) => {
                if (openBtn.dataset.modal && this.modals[openBtn.dataset.modal]) {
                    openBtn.addEventListener('click', (evt) => {
                        this.modals[openBtn.dataset.modal].show();
                        this.modals[openBtn.dataset.modal]._targetEl.dispatchEvent(new Event('modal-open', {modal: this.modals[openBtn.dataset.modal]}));
                    });
                }
            });

            document.querySelectorAll('.js-modal-close').forEach((closeBtn) => {
                if (closeBtn.dataset.modal && this.modals[closeBtn.dataset.modal]) {
                    closeBtn.addEventListener('click', (evt) => {
                        this.modals[closeBtn.dataset.modal].hide();
                        this.modals[closeBtn.dataset.modal]._targetEl.dispatchEvent(new Event('modal-close', {modal: this.modals[closeBtn.dataset.modal]}));
                    });
                }
            });
        });
    }

    _initDeleteButtons() {
        document.querySelectorAll('.js-delete-btn').forEach((btn) => {
            new DeleteButton({
                btn: btn,
            });
        })
    }
}
