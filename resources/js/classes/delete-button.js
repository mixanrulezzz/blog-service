import {Csrf} from "./csrf";

export default class DeleteButton {
    constructor({btn}) {
        this.button = btn;
        this.deleteHref = this.button.dataset.route;

        this._setEventListeners();
    }

    _setEventListeners() {
        this.button.addEventListener('click', () => {
            fetch(this.deleteHref, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    '_token': Csrf.getToken(),
                })
            })
                .then((response) => {
                    if (!response.ok) {
                        return response.text().then(text => { throw new Error(text) })
                    }

                    return response;
                })
                .then((data) => {
                    document.location.reload();
                })
                .catch((err) => {
                    console.log(err.message);
                });
        });
    }
}
