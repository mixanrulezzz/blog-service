export class Csrf {
    static getToken() {
        const csrfTokenInput = document.querySelector('input[name="_token"]');

        if (csrfTokenInput) {
            return csrfTokenInput.value;
        } else {
            return null;
        }
    }
}
