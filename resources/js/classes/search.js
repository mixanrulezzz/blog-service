import {Csrf} from "./csrf";

export default class Search {
    constructor({input, url, method, timeout, searchHandler, errorHandler}) {
        this.searchInput = input;
        this.csrfToken = Csrf.getToken();
        this.searchUrl = url;
        this.searchMethod = method ?? 'POST';
        this.searchTimeout = timeout;
        this.searchHandler = searchHandler;
        this.searchErrorHandler = errorHandler;
        this.setEventListeners();
    }

    setEventListeners()
    {
        let searchTimeoutObj = null;

        this.searchInput.addEventListener('input', (evt) => {
            clearTimeout(searchTimeoutObj);
            searchTimeoutObj = setTimeout(() => {
                this.search(this.searchInput.value);
            }, this.searchTimeout);
        });
    }

    search(searchValue)
    {
        fetch(this.searchUrl, {
            method: this.searchMethod,
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                word: searchValue,
                '_token': this.csrfToken,
            }),
        })
            .then((response) => {
                if (!response.ok) {
                    return response.text().then(text => { throw new Error(text) });
                }

                return response.json();
            })
            .then((data) => this.searchHandler(data))
            .catch((err) => {
                try {
                    this.searchErrorHandler(JSON.parse(err.message));
                } catch (e) {
                    this.searchErrorHandler(err.message);
                }
            })
    }
}
