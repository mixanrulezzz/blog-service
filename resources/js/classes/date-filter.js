export default class DateFilter {
    constructor({fromDateInput, toDateInput}) {
        this.fromDateInput = fromDateInput;
        this.toDateInput = toDateInput;

        this._init();
    }

    _init() {
        this._setEventListeners();
    }

    _setEventListeners() {
        this.fromDateInput.addEventListener('input', (evt) => {
            this._dateInputHandler(evt);
        });
        this.toDateInput.addEventListener('input', (evt) => {
            this._dateInputHandler(evt);
        });
    }

    _dateInputHandler(evt) {
        const fromValue = this.fromDateInput.value;
        const toValue = this.toDateInput.value;

        if (Date.parse(fromValue) > Date.parse(toValue)) {
            this.fromDateInput.value = toValue;
            this.toDateInput.value = fromValue;
        }
    }
}
