import ListWithDeleteButton from "../list/list-with-delete-button";

export default class AuthorsInBlog extends ListWithDeleteButton {
    initList() {
        this.getAllItems().forEach((element) => {
            element.querySelector('.js-delete-btn').addEventListener('click', this.deleteHandler);
        });
    }
}
