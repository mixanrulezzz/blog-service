import Form from "./classes/form";
import ModalWithMultiAddForm from "./classes/modal-with-multi-add-form";
import AuthorsInBlog from "./classes/author/authors-in-blog";

document.addEventListener("DOMContentLoaded", () => {
    const editBlogForm = document.querySelector('#edit-blog-form');
    const authorsToAddForm = document.querySelector('#authors-to-add-form');

    const editBlogFormObj = new Form({
        form: editBlogForm,
        submitHandler: (evt) => {
            const formData = editBlogFormObj.getCurFormData();
            const dataToSend = Object.fromEntries(formData.entries());
            delete dataToSend['authors[]'];
            dataToSend['authors'] = formData.getAll('authors[]');

            let editUrl = '/blog';
            let method = 'POST';
            if (formData.get('id')) {
                editUrl += '/' + formData.get('id');
                method = 'PUT';
            }

            editBlogFormObj
                .sendData({
                    url: editUrl,
                    data: dataToSend,
                    method: method,
                })
                .then((response) => {
                    document.location.href = response.url;
                })
                .catch((err) => {
                    try {
                        const errors = JSON.parse(err.message);
                        Object.keys(errors).forEach((inputName) => {
                            editBlogFormObj.showInputError({
                                inputName: inputName,
                                errorMessage: errors[inputName][0],
                            });
                        })
                    } catch (e) {
                        console.log(err.message);
                    }
                });
        }
    });

    const authorInBlogTemplate = document.querySelector('#author');
    const authorsInBlogContainer = document.querySelector('#blog-authors-container');
    const authorsInBlog = new AuthorsInBlog({
        itemTemplate: authorInBlogTemplate,
        listContainer: authorsInBlogContainer,
    });

    authorsInBlog.setDeleteHandler((evt) => {
        const authorItem = evt.target.closest('.js-list-item');
        authorItem.remove();
    });

    authorsInBlog.initList();

    const modalContainer = document.querySelector('#authors-modal');

    const addAuthorsModal = new ModalWithMultiAddForm({
        modalContainer: modalContainer,
        searchUrl: '/authors/search',
        searchTimeout: 500,
        submitHandler: (items) => {
            items.forEach((elem) => {
                const authorId = elem.querySelector('.js-item-value').value;
                const authorName = elem.querySelector('.js-item-text').textContent;
                authorsInBlog.addToList({value: authorId, text: authorName});
            });

            editBlogFormObj.getInputByName('authors').dispatchEvent(new Event('input', { bubbles: true }));
        }
    });

    addAuthorsModal.setPrepareSearchDataFunc((data) => {
        let preparedData = [];
        const authorsToAdd = (new FormData(authorsToAddForm)).getAll('authorsToAdd[]');
        const addedAuthors = (new FormData(editBlogForm)).getAll('authors[]');
        const authors = authorsToAdd.concat(addedAuthors);

        data.forEach((elem) => {
            preparedData.push({
                value: elem.value,
                text: elem.text,
                buttonState: authors.includes(elem.value.toString()) ? 'passive' : 'active',
            });
        });

        return preparedData;
    })
});
