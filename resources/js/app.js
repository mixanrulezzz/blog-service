import './bootstrap';

import Alpine from 'alpinejs';
import focus from '@alpinejs/focus';
import App from './classes/app';
window.Alpine = Alpine;

Alpine.plugin(focus);

Alpine.start();

window.App = new App();
