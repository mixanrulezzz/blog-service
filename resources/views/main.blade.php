<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Main') }}
        </h2>
    </x-slot>

    @section('scripts')
        @vite(['resources/js/main.js'])
    @endsection

    <div class="py-6">
        <x-filter-sort-form
            :sort-list="$sortList"
            :request="$request"
            :form-action="route('main')"
            :form-id="'search-posts'">
        </x-filter-sort-form>

        <div class="px-12">
            @foreach($posts as $post)
                <x-post.one :post="$post"></x-post.one>
            @endforeach

            {!! $posts->links() !!}
        </div>
    </div>
</x-app-layout>
