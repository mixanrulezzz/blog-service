<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('My blogs') }}
        </h2>
    </x-slot>

    <div class="py-6">
        <div class="px-12">
            <a href="{{ route('blog.new') }}">
                <div class="bg-green-500 py-2 px-4 w-fit rounded-lg">
                    <p class="text-white">{{ __('Create blog') }}</p>
                </div>
            </a>
            @if(!empty($userCreatedBlogs))
                <p class="pt-2 font-bold">{{ __('Blogs which you create') }}</p>
                @foreach($userCreatedBlogs as $blog)
                    <x-blog.one :blog="$blog"></x-blog.one>
                @endforeach
                {!! $userCreatedBlogs->links() !!}
            @endif

            @if(!empty($userAuthorBlogs))
                <p class="pt-2 font-bold">{{ __('Blogs where you are an author') }}</p>
                @foreach($userAuthorBlogs as $blog)
                    <x-blog.one :blog="$blog"></x-blog.one>
                @endforeach
                {!! $userAuthorBlogs->links() !!}
            @endif
        </div>
    </div>
</x-app-layout>
