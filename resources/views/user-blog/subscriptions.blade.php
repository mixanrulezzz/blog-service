<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('My subscriptions') }}
        </h2>
    </x-slot>

    <div class="py-6">
        <div class="px-12">
            @if(!empty($userSubscriptionBlogs))
                @foreach($userSubscriptionBlogs as $blog)
                    <x-blog.one :blog="$blog"></x-blog.one>
                @endforeach
                {!! $userSubscriptionBlogs->links() !!}
            @endif
        </div>
    </div>
</x-app-layout>
