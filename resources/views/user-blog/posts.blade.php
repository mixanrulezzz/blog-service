<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('My posts') }}
        </h2>
    </x-slot>

    <div class="py-6">
        <div class="px-12">
            <a href="{{ route('post.new') }}">
                <div class="bg-green-500 py-2 px-4 w-fit rounded-lg">
                    <p class="text-white">{{ __('Create post') }}</p>
                </div>
            </a>

            @foreach($userPosts as $post)
                <x-post.one :post="$post"></x-post.one>
            @endforeach

            {!! $userPosts->links() !!}
        </div>
    </div>
</x-app-layout>
