<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Blog list') }}
        </h2>
    </x-slot>

    @section('scripts')
        @vite(['resources/js/blog-list.js'])
    @endsection

    <div class="py-6">
        <x-filter-sort-form
            :sort-list="$sortList"
            :request="$request"
            :form-action="route('blog.list')"
            :form-id="'search-blogs'">
        </x-filter-sort-form>

        <div class="px-12">
            @foreach($blogs as $blog)
                <x-blog.one :blog="$blog"></x-blog.one>
            @endforeach
            {!! $blogs->links() !!}
        </div>
    </div>
</x-app-layout>
