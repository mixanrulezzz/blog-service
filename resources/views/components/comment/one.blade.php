<div class="bg-white mx-6 my-2 py-2 px-4 mx-auto rounded-md relative js-comment-one">
    <p class="py-2 font-bold">{{ $owner->name }}</p>
    <p class="py-2 js-comment-body">{{ $comment->body }}</p>

    @auth
        @if($comment->owner_id == Auth::id() || Auth::user()->is_admin)
            <div class="inline absolute right-0 top-0 py-3">
                <a href="javascript:void(0);" data-modal="edit-comment" data-comment-id="{{ $comment->id }}" data-post-id="{{ $postId }}" class="inline p-3 rounded-md hover:bg-gray-100 js-modal-open js-edit-comment">
                    <i class="fa-solid fa-pen-to-square inline"></i>
                    <p class="inline">{{ __('Edit') }}</p>
                </a>

                <a href="javascript:void(0);" data-route="{{ route('comment.delete', ['comment' => $comment->id]) }}" class="inline p-3 rounded-md hover:bg-gray-100 js-delete-btn">
                    <i class="fa-solid fa-trash-can inline"></i>
                    <p class="inline">{{ __('Delete') }}</p>
                </a>
            </div>
        @endif
    @endauth
</div>
