<div data-modal="edit-comment" tabindex="-1" aria-hidden="true" class="fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full js-modal-container">
    <div class="relative w-full max-w-md max-h-full">
        <!-- Modal content -->
        <form id="edit-comment-form">
            <div class="relative bg-white rounded-lg shadow">
                <!-- Modal header -->
                <div class="flex items-start justify-between p-4 border-b rounded-t">
                    <h3 class="text-xl font-semibold text-gray-900 hidden js-header-for-add">
                        {{__('Add comment')}}
                    </h3>
                    <h3 class="text-xl font-semibold text-gray-900 hidden js-header-for-edit">
                        {{__('Edit comment')}}
                    </h3>
                    <button type="button" data-modal="edit-comment" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center js-modal-close">
                        <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                        <span class="sr-only">Close modal</span>
                    </button>
                </div>
                <!-- Modal body -->
                @csrf
                <input type="hidden" name="post-id" value="{{ $postId }}">
                <input type="hidden" name="id">
                <div class="p-6">
                    <div class="bg-white py-2 px-4 mx-auto max-w-prose lg:text-xl js-input-container">
                        <label for="description" class="block mb-2 text-sm font-medium text-gray-900">{{__('Comment text')}}</label>
                        <textarea name="body" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 error:bg-red-50 error:border error:border-red-500 error:text-red-900 error:placeholder-red-700"></textarea>
                        <p class="mt-2 text-sm text-red-600 js-input-error-message"></p>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="flex items-center p-6 space-x-2 border-t border-gray-200 rounded-b">
                    <button type="submit" class="hidden text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center js-add-button">
                        {{ __('Add') }}
                    </button>

                    <button type="submit" class="hidden text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center js-edit-button">
                        {{ __('Edit') }}
                    </button>

                    <button type="reset" data-modal="edit-comment" class="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 js-modal-close">
                        {{ __('Cancel') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
