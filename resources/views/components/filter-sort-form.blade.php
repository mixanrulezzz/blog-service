<form action="{{ $formAction }}" method="GET" id="{{ $formId }}">
    <div class="px-12">
        <div class="relative">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <i class="fa-solid fa-magnifying-glass"></i>
            </div>
            <input
                type="search"
                id="default-search"
                name="word"
                value="{{ $request?->get('word') ?? '' }}"
                minlength="3"
                maxlength="255"
                class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500"
                placeholder="{{ __('Search') }}">
            <button type="submit" class="text-white absolute right-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2">
                {{ __('Search') }}
            </button>
        </div>
    </div>

    <div class="flex justify-between">
        <div class="flex pl-12 pr-2 py-3">
            <button type="button" class="js-filter-dropdown-btn px-3 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2.5 text-center inline-flex items-center">
                <p>{{ __('Filter') }}</p>
            </button>
            <div class="js-filter-dropdown-menu z-10 hidden bg-white divide-y divide-gray-100 rounded-lg shadow w-44">
                <ul class="py-2 text-sm text-gray-700" aria-labelledby="filterDropdownButton">
                    <li>
                        <p class="px-4">По дате:</p>
                    </li>
                    <li class="my-2">
                        <p class="inline ml-4 my-2 float-left">От:</p>
                        <input
                            type="date"
                            name="date-from"
                            max="{{ Date::now()->format('Y-m-d') }}"
                            value="{{ $request?->get('date-from') ?? '' }}"
                            class="inline-block float-right px-1 py-1 mr-2 my-1 w-auto text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500">
                    </li>
                    <li class="my-2">
                        <p class="inline ml-4 my-2 float-left">До:</p>
                        <input
                            type="date"
                            name="date-to"
                            max="{{ Date::now()->format('Y-m-d') }}"
                            value="{{ $request?->get('date-to') ?? '' }}"
                            class="inline-block float-right px-1 py-1 mr-2 my-1 w-auto text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500">
                    </li>
                    <li>
                        <button type="submit" class="text-white mx-2 my-1 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2">
                            {{ __('Filter') }}
                        </button>
                    </li>
                </ul>
            </div>
            <button type="button" class="js-sort-dropdown-btn px-3 mx-3 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2.5 text-center inline-flex items-center">
                @if($request?->get('side') == 'asc')
                    <i class="fa-solid fa-arrow-down-short-wide w-4"></i>
                @else
                    <i class="fa-solid fa-arrow-down-wide-short w-4"></i>
                @endif
                <p class="ml-1">{{ __('sort.' . ($request?->get('sort') ?? 'published_at')) }}</p>
            </button>
            <div class="js-sort-dropdown-menu z-10 hidden bg-white divide-y divide-gray-100 rounded-lg shadow w-44">
                <ul class="py-2 text-sm text-gray-700" aria-labelledby="filterDropdownButton">
                    @foreach($sortList as $value)
                        <li class="block js-sort-item" data-value="{{ $value }}">
                            <a href="javascript:void(0);" class="block px-4 py-2 hover:bg-gray-200 w-full js-side-dropdown-btn">
                                {{ __('sort.' . $value) }}
                            </a>

                            <div class="js-side-dropdown-menu z-10 hidden bg-white divide-y divide-gray-100 rounded-lg shadow w-12">
                                <ul class="py-2 text-sm text-gray-700" aria-labelledby="filterDropdownButton">
                                    <li class="block">
                                        <a href="javascript:void(0);" data-value="asc" class="block px-4 py-2 hover:bg-gray-200 w-full js-side-item">
                                            <i class="fa-solid fa-arrow-down-short-wide w-4"></i>
                                        </a>
                                        <a href="javascript:void(0);" data-value="desc" class="block px-4 py-2 hover:bg-gray-200 w-full js-side-item">
                                            <i class="fa-solid fa-arrow-down-wide-short w-4"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        @if($request->hasAny(['word', 'date-from', 'date-to']))
            <div class="flex grid pr-12 pl-2 py-3">
                <a href="javascript:void(0);" class="place-self-center js-filter-reset">Сбросить</a>
            </div>
        @endif
    </div>
</form>
