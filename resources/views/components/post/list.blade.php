@if($style == 'ordered')
    <ol>
@else
    <ul>
@endif

@foreach($items as $item)
    <li>{{ $item->content }}</li>

    @if(!empty($item->items))
        <x-post.list :style="$style" :items="$item->items"></x-post.list>
    @endif
@endforeach

@if($style == 'ordered')
    </ol>
@else
    </ul>
@endif
