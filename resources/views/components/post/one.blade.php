<div class="bg-white mx-6 my-2 py-2 px-4 mx-auto rounded-md relative js-post" data-id="{{ $post->id }}">
    <a href="{{ route('post.detail', ['post' => $post->id]) }}">
        <p class="font-bold">{{ $post->title }}</p>
    </a>
    <p class="text-sm italic py-2">
        @if($post->is_published)
            {{ __('Published :published_at', ['published_at' => \Carbon\Carbon::parse($post->published_at)->format('d.m.Y H:i:s')]) }}
        @else
            {{ __('Not published') }}
        @endif
    </p>
    <p class="text-sm italic">
        {{ __('Created :created_at', ['created_at' => $post->created_at->format('d.m.Y H:i:s')]) }}
    </p>

    @auth
        @if($post->owner_id == Auth::id() || Auth::user()->is_admin)
            <div class="inline absolute right-0 top-0 py-3">
                <a href="{{ route('post.edit', ['post' => $post->id]) }}" class="inline p-3 rounded-md hover:bg-gray-100">
                    <i class="fa-solid fa-pen-to-square inline"></i>
                    <p class="inline">{{ __('Edit') }}</p>
                </a>

                <a href="javascript:void(0);" data-route="{{ route('post.delete', ['post' => $post->id]) }}" class="inline p-3 rounded-md hover:bg-gray-100 js-delete-btn">
                    <i class="fa-solid fa-trash-can inline"></i>
                    <p class="inline">{{ __('Delete') }}</p>
                </a>
            </div>
        @endif
    @endauth
</div>
