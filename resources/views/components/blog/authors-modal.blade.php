<!-- Main modal -->
<div id="authors-modal" data-modal="authors-modal" tabindex="-1" aria-hidden="true" class="fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full js-modal-container">
    <div class="relative w-full max-w-md max-h-full">
        <!-- Modal content -->
        <div class="relative bg-white rounded-lg shadow">
            <!-- Modal header -->
            <div class="flex items-start justify-between p-4 border-b rounded-t">
                <h3 class="text-xl font-semibold text-gray-900">
                    {{__('Add new authors')}}
                </h3>
                <button type="button" data-modal="authors-modal" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center js-modal-close">
                    <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                    <span class="sr-only">Close modal</span>
                </button>
            </div>
            <!-- Modal body -->
            <div class="p-6">

                <label for="simple-search" class="sr-only">{{ __('Find authors') }}</label>
                <div class="relative w-full">
                    <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                        <i class="fa-solid fa-magnifying-glass"></i>
                    </div>
                    <input type="text" id="search-authors-input" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5 js-search-input" placeholder="{{ __('Find authors') }}" required>
                </div>

                <!-- Dropdown menu -->
                <div class="z-10 hidden bg-gray-100 rounded-lg shadow w-11/12 js-search-dropdown">
                    <ul class="max-h-48 p-3 overflow-y-auto text-sm js-search-result-container" aria-labelledby="search-authors-input">
                        <li class="js-search-error-block">
                            <div class="flex items-center pl-2 rounded">
                                <p class="w-full py-2 ml-2 text-sm font-medium text-gray-900 rounded js-error-text">{{ __('Please enter at least 3 characters to start the search') }}</p>
                            </div>
                        </li>

                        <li class="hidden js-empty-search-block">
                            <div class="flex items-center pl-2 rounded">
                                <p class="w-full py-2 ml-2 text-sm font-medium text-gray-900 rounded">{{ __('No results found') }}</p>
                            </div>
                        </li>

                        <template class="js-search-item-template">
                            <li class="js-list-item js-item-value">
                                <div x-data="{ show: false }" x-on:mouseover="show = true" x-on:mouseout="show = false" class="flex items-center pl-2 rounded hover:bg-gray-200 relative">
                                    <p class="w-full py-2 ml-2 text-sm font-medium text-gray-900 rounded js-item-text"></p>
                                    <a href="javascript:void(0);" class="absolute right-0 p-1 mx-3 rounded hover:bg-gray-300 data-passive:pointer-events-none js-add-button" :class="show || 'hidden'">
                                        <i class="js-icon fa-solid data-active:text-blue-600 data-passive:text-green-600"></i>
                                    </a>
                                </div>
                            </li>
                        </template>
                    </ul>
                </div>

                <div class="py-4">
                    <form id="authors-to-add-form">
                        <p class="text-sm font-semibold">{{ __('Users to add') }}</p>
                        <div class="min-h-[40px] max-h-[5.5rem] overflow-y-auto space-y-1 js-to-add-list-container">
                            <template class="js-to-add-item-template">
                                <div class="bg-gray-100 rounded p-2 inline-block js-list-item">
                                    <i class="inline fa-solid fa-user-plus text-blue-600 px-0.5"></i>
                                    <input type="hidden" name="authorsToAdd[]" class="js-item-value">
                                    <p class="inline js-item-text"></p>
                                    <a href="javascript:void(0);" class="px-0.5 inline rounded hover:bg-gray-300 js-delete-btn">
                                        <i class="js-icon fa-solid fa-xmark"></i>
                                    </a>
                                </div>
                            </template>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="flex items-center p-6 space-x-2 border-t border-gray-200 rounded-b">
                <button type="button" data-modal="authors-modal" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center js-submit-button js-modal-close">
                    {{ __('Add') }}
                </button>
                <button type="button" data-modal="authors-modal" class="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 js-reset-button js-modal-close">
                    {{ __('Cancel') }}
                </button>
            </div>
        </div>
    </div>
</div>
