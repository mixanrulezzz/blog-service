<div class="bg-white mx-6 my-2 py-2 px-4 mx-auto rounded-md relative">
    <a href="{{ route('blog.detail', ['blog' => $blog->id]) }}">
        <p class="font-bold">{{ $blog->title }}</p>
    </a>
    <p class="pt-2">{{ $blog->description }}</p>
    <p class="text-sm italic py-2">
        {{ __('Updated :updated_at', ['updated_at' => $blog->updated_at->format('d.m.Y H:i:s')]) }}
    </p>
    <p class="text-sm italic">
        {{ __('Created :created_at', ['created_at' => $blog->created_at->format('d.m.Y H:i:s')]) }}
    </p>
    @auth
        @if($blog->owner_id == Auth::id() || Auth::user()->is_admin)
            <div class="inline absolute right-0 top-0 py-3">
                <a href="{{ route('blog.edit', ['blog' => $blog->id]) }}" class="inline p-3 rounded-md hover:bg-gray-100">
                    <i class="fa-solid fa-pen-to-square inline"></i>
                    <p class="inline">{{ __('Edit') }}</p>
                </a>

                <a href="javascript:void(0);" data-route="{{ route('blog.delete', ['blog' => $blog->id]) }}" class="inline p-3 rounded-md hover:bg-gray-100 js-delete-btn">
                    <i class="fa-solid fa-trash-can inline"></i>
                    <p class="inline">{{ __('Delete') }}</p>
                </a>
            </div>
        @endif
    @endauth
</div>
