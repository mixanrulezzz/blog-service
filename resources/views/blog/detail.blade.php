<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="flex font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Blog') }}
            </h2>

            @auth
                @csrf
                <div class="flex js-blog-subscription-container {{ $isSubscribed ? 'unsubscribe' : 'subscribe'}}" data-blog-id="{{ $blogId }}">
                    <button class="js-blog-subscribe-btn flex text-white bg-gradient-to-r from-red-400 via-red-500 to-red-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-md text-sm px-5 py-0.5 text-center unsubscribe:hidden">
                        <p>{{ __('Subscribe') }}</p>
                    </button>

                    <button class="js-blog-unsubscribe-btn flex text-gray-800 bg-gray-100 hover:bg-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-md text-sm px-5 py-0.5 text-center subscribe:hidden">
                        <p>{{ __('Unsubscribe') }}</p>
                    </button>
                </div>
            @endauth
        </div>
    </x-slot>

    @auth
        @section('scripts')
            @vite(['resources/js/blog-subscribe.js'])
        @endsection
    @endauth

    <div class="py-6">
        <div class="px-12">
            @foreach($posts as $post)
                <x-post.one :post="$post"></x-post.one>
            @endforeach
            {!! $posts->links() !!}
        </div>
    </div>
</x-app-layout>
