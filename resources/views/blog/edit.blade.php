<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            @if(!empty($editBlog))
                {{ __('Edit blog #:id', ['id' => $editBlog->id]) }}
            @else
                {{ __('New blog') }}
            @endif
        </h2>
    </x-slot>

    @section('scripts')
        @vite(['resources/js/blog-edit.js'])
    @endsection

    <div class="py-6">
        <div class="px-12">
            <form id="edit-blog-form">
                @csrf
                @if(!empty($editBlog))
                    <input type="hidden" name="id" value="{{ $editBlog->id }}">
                @endif
                <div class="bg-white py-2 px-4 mx-auto max-w-prose lg:text-xl js-input-container">
                    <label for="title" class="block mb-2 text-sm font-medium text-gray-900">{{__('Title')}}</label>
                    <input type="text" name="title" id="title" value="{{ $editBlog->title ?? '' }}" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 error:bg-red-50 error:border error:border-red-500 error:text-red-900 error:placeholder-red-700">
                    <p class="mt-2 text-sm text-red-600 js-input-error-message"></p>
                </div>
                <div class="bg-white py-2 px-4 mx-auto max-w-prose lg:text-xl js-input-container">
                    <label for="description" class="block mb-2 text-sm font-medium text-gray-900">{{__('Description')}}</label>
                    <textarea name="description" id="description" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 error:bg-red-50 error:border error:border-red-500 error:text-red-900 error:placeholder-red-700">{{ $editBlog->description ?? '' }}</textarea>
                    <p class="mt-2 text-sm text-red-600 js-input-error-message"></p>
                </div>

                <div class="mx-auto max-w-prose lg:text-xl relative js-input-container">
                    <div class="bg-white py-2 px-4">
                        <input type="hidden" name="authors">
                        <button id="dropdownAuthorsButton" data-dropdown-toggle="dropdownAuthors" data-dropdown-placement="bottom" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2.5 text-center inline-flex items-center" type="button">
                            {{__('Blog authors')}}
                            <svg class="w-4 h-4 ml-2" aria-hidden="true" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path>
                            </svg>
                        </button>
                        <p class="mt-2 text-sm text-red-600 js-input-error-message"></p>
                    </div>

                    <div class="px-4 absolute left-0">
                        <!-- Dropdown menu -->
                        <div id="dropdownAuthors" class="z-10 hidden bg-white rounded-lg shadow w-60">
                            <ul class="h-48 py-2 overflow-y-auto text-gray-700" id="blog-authors-container" aria-labelledby="dropdownAuthorsButton">
                                @if(!empty($editBlog))
                                    @foreach($editBlog->authors()->get(['id', 'name']) as $author)
                                        <li class="js-list-item">
                                            <div x-data="{ show: false }" x-on:mouseover="show = true" x-on:mouseout="show = false" class="flex items-center px-4 py-2 hover:bg-gray-100 relative">
                                                <input type="hidden" name="authors[]" class="js-item-value" value="{{ $author->id }}">
                                                <p class="js-item-text">{{ $author->name }}</p>
                                                <a href="javascript:void(0);" class="absolute right-0 px-4 js-delete-btn" :class="show || 'hidden'">
                                                    <i class="fa-solid fa-user-minus text-red-500"></i>
                                                </a>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif

                                <template id="author">
                                    <li class="js-list-item">
                                        <div x-data="{ show: false }" x-on:mouseover="show = true" x-on:mouseout="show = false" class="flex items-center px-4 py-2 hover:bg-gray-100 relative">
                                            <input type="hidden" name="authors[]" class="js-item-value">
                                            <p class="js-item-text"></p>
                                            <a href="javascript:void(0);" class="absolute right-0 px-4 js-delete-btn" :class="show || 'hidden'">
                                                <i class="fa-solid fa-user-minus text-red-500"></i>
                                            </a>
                                        </div>
                                    </li>
                                </template>
                            </ul>
                            <a href="javascript:void(0);" data-modal="authors-modal" class="flex items-center p-3 text-sm font-medium text-blue-600 border-t border-gray-200 rounded-b-lg bg-gray-50 hover:bg-gray-100 js-modal-open">
                                <i class="fa-solid fa-user-plus pr-1"></i>
                                {{__('Add new authors')}}
                            </a>
                        </div>
                    </div>

                    <div class="py-2 px-4 mx-auto max-w-prose lg:text-xl">
                        <div class="max-w-prose">
                            <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 js-post-save-btn">
                                {{ __('Save') }}
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <x-blog.authors-modal></x-blog.authors-modal>
</x-app-layout>
