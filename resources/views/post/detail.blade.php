<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="flex font-semibold text-xl text-gray-800 leading-tight">
                @if(!empty($post))
                    {{ $post->title }}
                @else
                    {{ __('Post') }}
                @endif
            </h2>

            <div class="flex">
                <i class="fa-solid fa-eye inline self-center"></i>
                <p class="ml-1 inline self-center js-post-views-count">{{ $post->views_count }}</p>

                <button class="{{ $isLiked ? 'liked' : '' }} js-post-like-button inline self-center @auth hover:text-gray-600 liked:text-sky-600 liked:hover:text-sky-800 @endauth" @guest disabled @endguest>
                    <i class="ml-2 fa-solid fa-thumbs-up inline self-center"></i>
                    <p class="inline self-center js-post-likes-count text-gray-800">{{ $post->likes_count }}</p>
                </button>
            </div>
        </div>
    </x-slot>

    @section('scripts')
        @vite(['resources/js/post-detail.js'])
    @endsection

    @csrf
    <div class="py-6">
        <div class="px-12">
            <div class="mx-auto prose lg:prose-xl js-post-container" data-post-id="{{ $post->id }}">
                @foreach(json_decode($post->body)->blocks as $block)
                    @switch($block->type)
                        @case('paragraph')
                            <x-post.paragraph :text="$block->data->text"></x-post.paragraph>
                            @break
                        @case('header')
                            <x-post.header :level="$block->data->level" :text="$block->data->text"></x-post.header>
                            @break
                        @case('link')
                            <x-post.link :link="$block->data->link"></x-post.link>
                            @break
                        @case('raw')
                            <x-post.raw :raw="$block->data->html"></x-post.raw>
                            @break
                        @case('image')
                            <x-post.image :src="$block->data->url" :alt="$block->data->caption"></x-post.image>
                            @break
                        @case('list')
                            <x-post.list :style="$block->data->style" :items="$block->data->items"></x-post.list>
                            @break
                    @endswitch
                @endforeach
            </div>
        </div>
    </div>

    <div class="py-6">
        <div class="px-12 max-w-7xl mx-auto">
            <p class="font-bold">{{ __('Comments') }}:</p>

            @auth
                <a href="javascript:void(0);" data-modal="edit-comment" data-post-id="{{ $post->id }}" class="js-modal-open js-add-comment">
                    <div class="bg-green-500 my-2 py-2 px-4 w-fit rounded-lg">
                        <p class="text-white">{{ __('Add comment') }}</p>
                    </div>
                </a>
            @endauth
            @guest
                <p>{{ __('For add comment you need to authorize') }}</p>
            @endguest

            @if($comments->count() == 0)
                <p class="text-gray-500">{{ __('No comments') }}</p>
            @else
                @foreach($comments as $comment)
                    <x-comment.one :comment="$comment" :owner="$comment->owner()->first()" :post-id="$post->id"></x-comment.one>
                @endforeach

                {{ $comments->links() }}
            @endif

        </div>
    </div>

    <x-comment.edit-modal :post-id="$post->id"></x-comment.edit-modal>
</x-app-layout>
