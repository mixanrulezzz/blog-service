<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            @if(!empty($editPost))
                {{ __('Edit post #:id', ['id' => $editPost->id]) }}
            @else
                {{ __('New post') }}
            @endif

        </h2>
    </x-slot>

    @section('scripts')
        @vite(['resources/js/post-edit.js'])
    @endsection

    <div class="py-6">
        <div class="px-12">
            <form id="editPostForm">
                @csrf
                @if(!empty($editPost))
                    <input type="hidden" name="id" value="{{ $editPost->id }}">
                @endif
                <div class="bg-white py-2 px-4 mx-auto max-w-prose lg:text-xl js-input-container">
                    <label for="postTitle" class="block mb-2 text-sm font-medium text-gray-900">{{__('Title')}}</label>
                    <input type="text" name="title" id="postTitle" value="{{ $editPost->title ?? '' }}" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 error:bg-red-50 error:border error:border-red-500 error:text-red-900 error:placeholder-red-700">
                    <p class="mt-2 text-sm text-red-600 js-input-error-message"></p>
                </div>
                <div class="flex bg-white py-2 px-4 mx-auto max-w-prose lg:text-xl">
                    <div class="flex items-center h-5" x-data="{ is_published: {{ $editPost->is_published ?? false }}}">
                        <input id="postIsPublished" type="checkbox" name="is_published" value="Y" :checked="is_published" class="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-blue-300">
                    </div>
                    <label for="postIsPublished" class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">{{__('Is published')}}</label>
                </div>
                <div class="bg-white py-2 px-4 mx-auto max-w-prose lg:text-xl js-input-container">
                    <label for="blog-id" class="block mb-2 text-sm font-medium text-gray-900">{{ __('Blog') }}</label>
                    <select id="blog-id" name="blog_id" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 error:bg-red-50 error:border error:border-red-500 error:text-red-900 error:placeholder-red-700" required>
                        <option>{{ __('Choose a blog') }}</option>
                        @foreach($blogsAuthor as $blog)
                            <option value="{{ $blog->id }}" {{ !empty($editPost) && $editPost->blog_id == $blog->id ? 'selected' : '' }}>#{{ $blog->id }} {{ $blog->title }}</option>
                        @endforeach
                    </select>
                    <p class="mt-2 text-sm text-red-600 js-input-error-message"></p>
                </div>
                <div class="bg-white py-2 px-4 mx-auto max-w-prose lg:text-xl">
                    <label class="block mb-2 text-sm font-medium text-gray-900">{{__('Post')}}</label>
                </div>
                <div class="mx-auto prose lg:prose-xl">
                    <div class="bg-white" id="editorjs" data-data="{{ $editPost->body ?? '' }}"></div>

                    <div class="max-w-prose">
                        <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 js-post-save-btn">
                            {{ __('Save') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>
