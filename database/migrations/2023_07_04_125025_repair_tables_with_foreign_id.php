<?php

use App\Models\Blog;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('blog_authors', function (Blueprint $table) {
            $table->dropForeignIdFor(Blog::class, 'blog_id');
            $table->dropForeignIdFor(User::class, 'author_id');

            $table->foreign('blog_id')->references('id')->on('blogs')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign('author_id')->references('id')->on('users')->cascadeOnDelete()->cascadeOnUpdate();
        });

        Schema::table('blogs', function (Blueprint $table) {
            $table->dropForeignIdFor(User::class, 'owner_id');
            $table->bigInteger('owner_id')->unsigned()->nullable()->change();

            $table->foreign('owner_id')->references('id')->on('users')->nullOnDelete()->cascadeOnUpdate();
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeignIdFor(User::class, 'owner_id');
            $table->bigInteger('owner_id')->unsigned()->nullable()->change();

            $table->foreign('owner_id')->nullable()->change()->references('id')->on('users')->nullOnDelete()->cascadeOnUpdate();
        });

        Schema::table('comments', function (Blueprint $table) {
            $table->dropForeignIdFor(User::class, 'owner_id');

            $table->foreign('owner_id')->references('id')->on('users')->cascadeOnDelete()->cascadeOnUpdate();
        });

        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropForeignIdFor(User::class, 'user_id');
            $table->dropForeignIdFor(Blog::class, 'blog_id');

            $table->foreign('blog_id')->references('id')->on('blogs')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('blog_authors', function (Blueprint $table) {
            $table->dropForeignIdFor(Blog::class, 'blog_id');
            $table->dropForeignIdFor(User::class, 'author_id');

            $table->foreign('blog_id')->references('id')->on('blogs');
            $table->foreign('author_id')->references('id')->on('users');
        });

        Schema::table('blogs', function (Blueprint $table) {
            $table->dropForeignIdFor(User::class, 'owner_id');
            $table->bigInteger('owner_id')->unsigned()->change();

            $table->foreign('owner_id')->references('id')->on('users');
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeignIdFor(User::class, 'owner_id');
            $table->bigInteger('owner_id')->unsigned()->change();

            $table->foreign('owner_id')->references('id')->on('users');
        });

        Schema::table('comments', function (Blueprint $table) {
            $table->dropForeignIdFor(User::class, 'owner_id');

            $table->foreign('owner_id')->references('id')->on('users');
        });

        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropForeignIdFor(User::class, 'user_id');
            $table->dropForeignIdFor(Blog::class, 'blog_id');

            $table->foreign('blog_id')->references('id')->on('blogs');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }
};
