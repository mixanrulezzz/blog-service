<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('blogs_title_tsvector', function (Blueprint $table) {
            $table->foreignId('id')->constrained('blogs', 'id')->cascadeOnDelete()->cascadeOnUpdate();
            $table->primary('id');
            $table->addColumn('tsvector', 'title_tsvector');
            $table->index('title_tsvector', 'idx_gin_blogs_title_tsvector', 'gin');
        });

        // Заполнение таблицы данными
        DB::unprepared('
            INSERT INTO blogs_title_tsvector(id, title_tsvector)
            SELECT id, to_tsvector_multilang("title") as title_tsvector FROM blogs;
        ');

        // Триггер для добавления или изменения значений векторов при создании или изменении имени пользователя
        DB::unprepared('
            CREATE OR REPLACE FUNCTION update_blogs_title_tsvector()
                RETURNS TRIGGER
                LANGUAGE PLPGSQL
            AS
            $$
            BEGIN
                IF OLD is NULL THEN
                    INSERT INTO blogs_title_tsvector(id, title_tsvector)
                    VALUES(NEW.id, to_tsvector_multilang(NEW.title));
                ELSE
                    UPDATE blogs_title_tsvector
                    SET title_tsvector = to_tsvector_multilang(NEW.title)
                    WHERE id = NEW.id;
                END IF;

                RETURN NULL;
            END;
            $$;

            CREATE TRIGGER update_blogs_title_tsvector AFTER INSERT OR UPDATE OF title ON blogs FOR EACH ROW
                EXECUTE PROCEDURE update_blogs_title_tsvector();
        ');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::unprepared('DROP TRIGGER IF EXISTS update_blogs_title_tsvector ON blogs;');
        DB::unprepared('DROP FUNCTION IF EXISTS update_blogs_title_tsvector;');

        Schema::dropIfExists('blogs_title_tsvector');
    }
};
