<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Создадим функцию для подсчета векторов для нескольких языков: Русский и Английский
        DB::unprepared(
            "CREATE FUNCTION to_tsvector_multilang(text) RETURNS TSVECTOR AS $$
                    SELECT to_tsvector('english', $1) ||
                           to_tsvector('russian', $1) ||
                           to_tsvector('simple', $1)
                    $$ LANGUAGE sql IMMUTABLE;"
        );

        // Отдельная таблица для векторов, чтобы они не считались при запросе
        Schema::create('users_name_tsvector', function (Blueprint $table) {
            $table->foreignId('id')->constrained('users', 'id')->cascadeOnDelete()->cascadeOnUpdate();
            $table->primary('id');
            $table->addColumn('tsvector', 'name_vector');
        });

        // Создадим индекс для поля с tsvector
        DB::unprepared('
            CREATE INDEX idx_gin_name_vector
            ON users_name_tsvector
            USING gin ("name_vector");
        ');

        // Заполнение таблицы данными
        DB::unprepared('
            INSERT INTO users_name_tsvector(id, name_vector)
            SELECT id, to_tsvector_multilang("name") as name_vector FROM users;
        ');

        // Триггер для добавления или изменения значений векторов при создании или изменении имени пользователя
        DB::unprepared('
            CREATE OR REPLACE FUNCTION update_users_name_tsvector()
                RETURNS TRIGGER
                LANGUAGE PLPGSQL
            AS
            $$
            BEGIN
                IF OLD is NULL THEN
                    INSERT INTO users_name_tsvector(id, name_vector)
                    VALUES(NEW.id, to_tsvector_multilang(NEW.name));
                ELSE
                    UPDATE users_name_tsvector
                    SET name_vector = to_tsvector_multilang(NEW.name)
                    WHERE id = NEW.id;
                END IF;

                RETURN NULL;
            END;
            $$;

            CREATE TRIGGER update_users_name_tsvector AFTER INSERT OR UPDATE OF name ON users FOR EACH ROW
                EXECUTE PROCEDURE update_users_name_tsvector();
        ');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::unprepared('DROP TRIGGER IF EXISTS update_users_name_tsvector ON users;');
        DB::unprepared('DROP FUNCTION IF EXISTS update_users_name_tsvector;');

        Schema::drop('users_name_tsvector');

        DB::unprepared('DROP FUNCTION IF EXISTS to_tsvector_multilang;');
    }
};
