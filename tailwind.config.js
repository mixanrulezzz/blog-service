import defaultTheme from 'tailwindcss/defaultTheme';
import forms from '@tailwindcss/forms';
import typography from '@tailwindcss/typography';
const plugin = require('tailwindcss/plugin');

/** @type {import('tailwindcss').Config} */
export default {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './node_modules/flowbite/**/*.js',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Figtree', ...defaultTheme.fontFamily.sans],
            },
        },
        data: {
            active: 'ui="active"',
            passive: 'ui="passive"',
        },
    },

    plugins: [
        forms, typography, require('flowbite/plugin'),
        plugin(function({ addVariant }) {
            addVariant('error', '.error &')
            addVariant('not-error', ':not(.error) &')
            addVariant('subscribe', '.subscribe &')
            addVariant('unsubscribe', '.unsubscribe &')
            addVariant('liked', ['&.liked', '.liked &'])
        })
    ],
};
